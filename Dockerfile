FROM registry.gitlab.com/anatomicjc/docker-images/alpine:latest

COPY public /usr/share/nginx/html
COPY nginx/default.conf /default.conf
COPY nginx/nginx.conf /nginx.conf
RUN apk upgrade --no-cache; \
    apk add --no-cache nginx ; \
    mv /default.conf /etc/nginx/http.d/default.conf ; \
    mv /nginx.conf /etc/nginx/nginx.conf ; \
    ln -sf /dev/stdout /var/log/nginx/access.log ; \
    ln -sf /dev/stderr /var/log/nginx/error.log ; \
    # Distroless magic 
    find /sbin /bin /usr/bin /usr/local/bin/ -type l  -exec busybox rm -rf {} \;; \
    busybox rm /sbin/apk /bin/busybox

USER nginx
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
