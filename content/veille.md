---
title: "Veille"
author: JC
date: 2021-11-27T15:29:10+01:00
url: /veille
ShowToc: true
TocOpen: true
---

J'utilise pour ma veille [Wallabag](https://github.com/wallabag/wallabag), un "Read It Later" qui me permet de sauvegarder le contenu texte des pages web qui m'intressent, sans tout le superflu (bandeaux de pub, gots CSS douteux, ...).

Vous pouvez cliquer sur le lien d'origine ou lire directement via Wallabag.


## 2fa

2020-10-02: [authelia/authelia](https://github.com/authelia/authelia) | [Mirror link](https://wallabag.open-web.fr/share/601ea4b5db8829.30104712)

## ansible

2021-03-01: [Ansible 3.0 - Quels changements](https://blog.stephane-robert.info/post/ansible-3.0-nouveautes-changements/) | [Mirror link](https://wallabag.open-web.fr/share/603cb3a14f7961.54767346)

2021-02-24: [Run Multiple Ansible Versions Side by Side Using Python 3 Virtual Environments](https://www.simplygeek.co.uk/2019/08/23/run-multiple-ansible-versions-side-by-side-using-python-3-virtual-environments/) | [Mirror link](https://wallabag.open-web.fr/share/603615270143f9.45943214)

2020-03-03: [How to Speed Up Your Ansible Playbooks Over 600%](https://www.toptechskills.com/ansible-tutorials-courses/speed-up-ansible-playbooks-pipelining-mitogen/) | [Mirror link](https://wallabag.open-web.fr/share/601feda8331329.01936482)

2019-12-07: [Contributing back to Ansible  flexible secrets with some Sops](https://blog.arduino.cc/2019/12/04/contributing-back-to-ansible-flexible-secrets-with-some-sops/) | [Mirror link](https://wallabag.open-web.fr/share/601ff0a08c3114.49086558)

## boulot

2019-02-26: [Journendentravail,nounjournenauntravailn?n-nHowTommynDev](http://dev.howtommy.net/?d=2019/02/25/09/53/57-journee-de-travail-ou-journee-au-travail) | [Mirror link](https://wallabag.open-web.fr/share/60f98fd95b6b01.98689682)

## cka

2021-03-01: [Kubernetes Journey??CKA / CKAD Exam Tips](https://itnext.io/kubernetes-journey-cka-ckad-exam-tips-ff73e4672833?gi=c502962b36b) | [Mirror link](https://wallabag.open-web.fr/share/603ded251fa9d9.06359923)

2021-02-26: [Be fast with Kubectl 1.19 CKAD/CKA](https://medium.com/faun/be-fast-with-kubectl-1-18-ckad-cka-31be00acc443) | [Mirror link](https://wallabag.open-web.fr/share/6038ccb03d4b00.28179849)

2021-02-19: [Yet Another Certified Kubernetes Administrator (CKA) Exam Guideline](https://medium.com/codable/yet-another-certified-kubernetes-administrator-cka-exam-guideline-d35f4952add) | [Mirror link](https://wallabag.open-web.fr/share/6030d302954a19.03116058)

2020-07-16: [Certified Kubernetes Administrator (CKA) with v1.18](https://medium.com/@apurvbhandari/certified-kubernetes-administrator-cka-with-v1-18-ab641bb4745f) | [Mirror link](https://wallabag.open-web.fr/share/601fea54e57bb0.17293706)

2020-05-19: [Comment j'ai obtenu la certification Kubernetes CKAD](https://thibault-lereste.fr/2020/05/certification-kubernetes-ckad/) | [Mirror link](https://wallabag.open-web.fr/share/601feafa655ae3.40417813)

2019-12-09: [Passing Certified Kubernetes Administrator: My lessons learned](https://medium.com/faun/passing-certified-kubernetes-administrator-exam-tips-d5107d8e3e7b) | [Mirror link](https://wallabag.open-web.fr/share/601ff068c2b015.15949877)

## coaching

2021-01-17: [5 exercices pour pratiquer les accords toltques](https://apprendreaeduquer.fr/exercices-pratiquer-accords-tolteques/) | [Mirror link](https://wallabag.open-web.fr/share/601e9a8e06bcb8.52105866)

2020-10-11: [7 Code Virtues Explained](https://industriallogic.com/blog/code-virtues-explained/) | [Mirror link](https://wallabag.open-web.fr/share/601ea36d7773a6.74327435)

2020-08-10: [Je ne vois que ce que je crois!](https://www.annelaure-nouvion.com/je-ne-vois-que-ce-que-je-crois/) | [Mirror link](https://wallabag.open-web.fr/share/601ea6111f81e4.01887173)

2020-02-28: [Je ne sais pas bien nager, ma plus longue sortie vlo fait 20km et je naime pas courir. Voila pourquoi jai dcid de minscrire sur un Iron Man"?](https://www.linkedin.com/pulse/je-ne-sais-pas-bien-nager-ma-plus-longe-sortie-vlo-olivier-delanne) | [Mirror link](https://wallabag.open-web.fr/share/601fedd01c76e3.43164965)

2019-12-10: [Les 4 accords Toltques](https://www.cadremploi.fr/editorial/conseils/conseils-carriere/detail/article/comment-les-accords-tolteques-inspirent-les-managers.html?_lrsc=2a52297d-04f8-46e3-8a7b-539add5da560) | [Mirror link](https://wallabag.open-web.fr/share/601ff039841920.77318669)

## containers

2020-12-09: [Container runtimes: clarity](https://medium.com/cri-o/container-runtimes-clarity-342b62172dc3) | [Mirror link](https://wallabag.open-web.fr/share/601ea1a42d8c90.98002832)

2020-08-10: [Docker vs CRI-O vs Containerd | ComputingForGeeks](https://computingforgeeks.com/docker-vs-cri-o-vs-containerd/) | [Mirror link](https://wallabag.open-web.fr/share/601ea672ae2092.02187237)

2020-07-13: [Scan Docker image vulnerabilities using Clair, Klar, Docker Registry and Traefik](https://medium.com/@edgar.halbert/scan-docker-image-vulnerabilities-using-clair-klar-docker-registry-and-traefik-23ba1e21217) | [Mirror link](https://wallabag.open-web.fr/share/601fea7c0e8397.84822007)

## divers

2020-07-30: [Au secours, le mtier d'Ops va disparatre ! - Zwindler's Reflection](https://blog.zwindler.fr/2020/07/29/au-secours-le-metier-dops-va-disparaitre/) | [Mirror link](https://wallabag.open-web.fr/share/601fe98d0cf3f6.13341428)

2020-03-13: [GitLab's Guide to All-Remote](https://about.gitlab.com/company/culture/all-remote/guide/) | [Mirror link](https://wallabag.open-web.fr/share/601fec52cca627.28840310)

2020-03-04: [How 1500 bytes became the MTU of the internet](https://blog.benjojo.co.uk/post/why-is-ethernet-mtu-1500) | [Mirror link](https://wallabag.open-web.fr/share/601feceaafbc37.33852290)

2020-02-13: [Les secrets du nombre 42](https://www.pourlascience.fr/sd/mathematiques/les-secrets-du-nombre-42-18744.php&from=SMA20TRA&fbclid=IwAR3MAoR0eka-lyuXksT_6L8FuVmKCnDFKySeBoKsM8nmZGR_putwe7ThxdI) | [Mirror link](https://wallabag.open-web.fr/share/601fee82182f39.65389756)

## docker

2020-12-23: [Pulling cached Docker Hub images](https://cloud.google.com/container-registry/docs/pulling-cached-images) | [Mirror link](https://wallabag.open-web.fr/share/601e9fb93ad4a1.21310295)

## kubernetes

2021-03-28: [10 Kubernetes Security Context settings you should understand | Snyk](https://snyk.io/blog/10-kubernetes-security-context-settings-you-should-understand/) | [Mirror link](https://wallabag.open-web.fr/share/60607f3d554e69.97723692)

2021-03-01: [Kubernetes Journey??CKA / CKAD Exam Tips](https://itnext.io/kubernetes-journey-cka-ckad-exam-tips-ff73e4672833?gi=c502962b36b) | [Mirror link](https://wallabag.open-web.fr/share/603ded251fa9d9.06359923)

2021-03-01: [Extending applications on Kubernetes with multi-container pods](https://learnk8s.io/sidecar-containers-patterns) | [Mirror link](https://wallabag.open-web.fr/share/603ded08bc6351.92727964)

2021-02-26: [Be fast with Kubectl 1.19 CKAD/CKA](https://medium.com/faun/be-fast-with-kubectl-1-18-ckad-cka-31be00acc443) | [Mirror link](https://wallabag.open-web.fr/share/6038ccb03d4b00.28179849)

2021-02-19: [Yet Another Certified Kubernetes Administrator (CKA) Exam Guideline](https://medium.com/codable/yet-another-certified-kubernetes-administrator-cka-exam-guideline-d35f4952add) | [Mirror link](https://wallabag.open-web.fr/share/6030d302954a19.03116058)

2021-02-17: [Mettre  jour le CA de Kubernetes, "the hard way" - Zwindler's Reflection](https://blog.zwindler.fr/2021/02/15/mettre-a-jour-le-ca-de-kubernetes-the-hard-way/) | [Mirror link](https://wallabag.open-web.fr/share/602cc3e52c84b4.99072166)

2021-01-03: [Radio France optimise ses pipelines CI/CD pour Kubernetes](https://www.lemagit.fr/etude/Radio-France-optimise-ses-pipelines-CI-CD-pour-Kubernetes) | [Mirror link](https://wallabag.open-web.fr/share/601e9de4bf04a0.60708888)

2021-01-03: [Constructing YAML with Kubectl only](https://itnext.io/constructing-yaml-with-kubectl-only-15b3fbff7485?gi=d504d0bae04e) | [Mirror link](https://wallabag.open-web.fr/share/601e9e3f248713.81400902)

2020-12-28: [Bare-metal Kubernetes with K3s](https://blog.alexellis.io/bare-metal-kubernetes-with-k3s/) | [Mirror link](https://wallabag.open-web.fr/share/601e9f5e26be88.00651909)

2020-12-09: [Container runtimes: clarity](https://medium.com/cri-o/container-runtimes-clarity-342b62172dc3) | [Mirror link](https://wallabag.open-web.fr/share/601ea1a42d8c90.98002832)

2020-12-07: [Wait, Docker is deprecated in Kubernetes now? What do I do?](https://dev.to/inductor/wait-docker-is-deprecated-in-kubernetes-now-what-do-i-do-e4m#%3A%7E%3Atext%3DYes%2C+it+is+true%2E%2Cis+now+deprecated+in+Kubernetes%2E%26text%3DDocker+support+in+the+kubelet%2Cissues+in+the+Kubernetes+community) | [Mirror link](https://wallabag.open-web.fr/share/601ea1c46c4176.43311359)

2020-11-13: [A First Look at Portainer 2.0 CE  Now with Kubernetes Support -](https://collabnix.com/a-first-look-at-portainer-2-0-ce-now-with-kubernetes-support/) | [Mirror link](https://wallabag.open-web.fr/share/601ea21f654a55.26312505)

2020-10-28: [Pod rescheduling after a node failure with RKE and Kubernetes](https://medium.com/01001101/pod-rescheduling-after-a-node-failure-with-rke-and-kubernetes-ed11cf3dbeb4) | [Mirror link](https://wallabag.open-web.fr/share/601ea2fbc065e9.33710696)

2020-10-21: [Minecraft as a k8s admin tool](https://blog.usejournal.com/minecraft-as-a-k8s-admin-tool-cf16f890de42?gi=de1c54f495a1) | [Mirror link](https://wallabag.open-web.fr/share/601ea312122b72.80277096)

2020-09-14: [Crer un cluster Kubernetes local avec Vagrant](https://tferdinand.net/creer-un-cluster-kubernetes-local-avec-vagrant/) | [Mirror link](https://wallabag.open-web.fr/share/601ea50aa3e155.37250908)

2020-09-08: [Deploy a secure etcd cluster](https://pcocc.readthedocs.io/en/latest/deps/etcd-production.html) | [Mirror link](https://wallabag.open-web.fr/share/601ea5350f7244.90610143)

2020-08-10: [Docker vs CRI-O vs Containerd | ComputingForGeeks](https://computingforgeeks.com/docker-vs-cri-o-vs-containerd/) | [Mirror link](https://wallabag.open-web.fr/share/601ea672ae2092.02187237)

2020-07-31: [CRD is just a table in Kubernetes](https://itnext.io/crd-is-just-a-table-in-kubernetes-13e15367bbe4?gi=cd755c309870) | [Mirror link](https://wallabag.open-web.fr/share/601fe9533998e9.86840372)

2020-07-29: [Introducing Kubelabs  An Ultimate Kubernetes101 Workshop -](https://collabnix.com/kickstart-your-kubernetes101-journey-with-kubelabs/) | [Mirror link](https://wallabag.open-web.fr/share/601fe9a7df25b2.40828617)

2020-07-24: [JSONPath Support](https://kubernetes.io/docs/reference/kubectl/jsonpath/) | [Mirror link](https://wallabag.open-web.fr/share/601fe9d9f269c1.45408552)

2020-07-19: [k3d + k3s = k8s perfect match for dev and testing](https://en.sokube.ch/post/k3d-k3s-k8s-perfect-match-for-dev-and-testing) | [Mirror link](https://wallabag.open-web.fr/share/601fea20251962.73617010)

2020-07-17: [Don't Memorize Kubernetes API Resources. Use these Two kubectl Commands instead! | Austin Dewey](https://austindewey.com/2020/06/21/dont-memorize-kubernetes-api-resources-use-these-two-kubectl-commands-instead/) | [Mirror link](https://wallabag.open-web.fr/share/601fea3df38729.38952042)

2020-07-16: [Certified Kubernetes Administrator (CKA) with v1.18](https://medium.com/@apurvbhandari/certified-kubernetes-administrator-cka-with-v1-18-ab641bb4745f) | [Mirror link](https://wallabag.open-web.fr/share/601fea54e57bb0.17293706)

2020-05-19: [Comment j'ai obtenu la certification Kubernetes CKAD](https://thibault-lereste.fr/2020/05/certification-kubernetes-ckad/) | [Mirror link](https://wallabag.open-web.fr/share/601feafa655ae3.40417813)

2020-04-29: [Kubernetes Multi-AZ deployments Using Pod Anti-Affinity](https://www.verygoodsecurity.com/blog/posts/kubernetes-multi-az-deployments-using-pod-anti-affinity/) | [Mirror link](https://wallabag.open-web.fr/share/601feb4ba336f5.09356853)

2020-03-10: [&ldquo;Let&rsquo;s use Kubernetes!&rdquo; Now you have 8 problems](https://pythonspeed.com/articles/dont-need-kubernetes/) | [Mirror link](https://wallabag.open-web.fr/share/601fecb13909b2.05446615)

2020-02-18: [Deploy A Ceph Cluster On Kubernetes With Rook](https://itnext.io/deploy-a-ceph-cluster-on-kubernetes-with-rook-d75a20c3f5b1?gi=ca4d9c94b8c0) | [Mirror link](https://wallabag.open-web.fr/share/601fee62b8f999.52185079)

2020-01-25: [Introducing Kubelabs  An Ultimate Kubernetes101 Workshop  Collabnix](http://collabnix.com/kickstart-your-kubernetes101-journey-with-kubelabs/) | [Mirror link](https://wallabag.open-web.fr/share/601fef3c04cc41.42329468)

2020-01-23: [Check your Helm deployments!](https://medium.com/polarsquad/check-your-helm-deployments-ffe26014804) | [Mirror link](https://wallabag.open-web.fr/share/601fef5c4ab403.05892022)

2019-12-24: [Kubernetes production best practices](https://learnk8s.io/production-best-practices) | [Mirror link](https://wallabag.open-web.fr/share/601ff01e024631.43497105)

2019-12-09: [Passing Certified Kubernetes Administrator: My lessons learned](https://medium.com/faun/passing-certified-kubernetes-administrator-exam-tips-d5107d8e3e7b) | [Mirror link](https://wallabag.open-web.fr/share/601ff068c2b015.15949877)

## linux

2020-03-16: [Que signifie &quot;&gt; /dev/null 2&gt;&amp;1&quot; ? - La Lanterne Rouge](http://lanterne-rouge.over-blog.org/article-que-signifie-dev-null-2-1-70233357.html) | [Mirror link](https://wallabag.open-web.fr/share/601fec1e1f4855.28375371)

2020-02-02: [In defence of swap: common misconceptions](https://chrisdown.name/2018/01/02/in-defence-of-swap.html) | [Mirror link](https://wallabag.open-web.fr/share/601fef13ad0025.73973780)

## mongodb

2020-11-30: [MongoDB: Le guide pour utiliser la database NoSQL de rfrence en 2020](https://practicalprogramming.fr/mongodb/) | [Mirror link](https://wallabag.open-web.fr/share/601ea1f9e7cfd4.23793453)

## nginx

2021-01-29: [Nginx and LDAP Authentication](https://warlord0blog.wordpress.com/2020/07/11/nginx-and-ldap-authentication/) | [Mirror link](https://wallabag.open-web.fr/share/601e9d75c55eb9.60744275)

## postgresql

2020-06-04: [la streaming replication en 12.  Capdata TECH BLOG](https://blog.capdata.fr/index.php/postgresql-la-streaming-replication-en-12/) | [Mirror link](https://wallabag.open-web.fr/share/601feac557f213.13701580)

## python

2021-02-24: [Why you should use pyenv + Pipenv for your Python projects | Hacker Noon](https://hackernoon.com/reaching-python-development-nirvana-bb5692adf30c) | [Mirror link](https://wallabag.open-web.fr/share/60364e9ce5e116.54049812)

2021-02-24: [Run Multiple Ansible Versions Side by Side Using Python 3 Virtual Environments](https://www.simplygeek.co.uk/2019/08/23/run-multiple-ansible-versions-side-by-side-using-python-3-virtual-environments/) | [Mirror link](https://wallabag.open-web.fr/share/603615270143f9.45943214)

2020-03-10: [&ldquo;Let&rsquo;s use Kubernetes!&rdquo; Now you have 8 problems](https://pythonspeed.com/articles/dont-need-kubernetes/) | [Mirror link](https://wallabag.open-web.fr/share/601fecb13909b2.05446615)

## restic

2019-12-29: [Append-only mode with S3 (Wasabi)](https://forum.restic.net/t/append-only-mode-with-s3-wasabi/845?_escaped_fragment_=) | [Mirror link](https://wallabag.open-web.fr/share/601fefbd425aa4.85218590)

2019-12-29: [Economical append-only offsite backups with restic and Wasabi on Debian 10](https://www.howtoforge.com/economical-append-only-offsite-backups-with-restic-and-wasabi-debian/#-set-the-data-bucket-to-be-immutable-appendonly) | [Mirror link](https://wallabag.open-web.fr/share/601feff0c9b7e7.47014610)

## systemd

2020-12-10: [Tutorial: Logging with...](https://sematext.com/blog/journald-logging-tutorial/) | [Mirror link](https://wallabag.open-web.fr/share/601ea1608aa8a1.50445949)

## terraform

2020-10-31: [250 Practice Questions For Terraform Associate Certification](https://medium.com/bb-tutorials-and-thoughts/250-practice-questions-for-terraform-associate-certification-7a3ccebe6a1a) | [Mirror link](https://wallabag.open-web.fr/share/601ea27be52c78.78132459)

2020-04-04: [Structurer son projet Terraform](https://blog.d2si.io/2020/02/13/structurer-son-projet-terraform/) | [Mirror link](https://wallabag.open-web.fr/share/601febf21a08d1.36432897)

## traefik

2021-02-07: [Traefik 2 : Rang A+ sur SSLLABS et SecurityHeaders](https://www.grottedubarbu.fr/traefik-2-rang-a-sur-ssllabs-et-securityheaders/) | [Mirror link](https://wallabag.open-web.fr/share/60280655df2108.03235478)

2020-03-03: [Traefik v2, configuration en mode &quot;dynamique&quot; et meilleure configuration TLS possible](https://computerz.solutions/traefik-v2-dynamique-ssl/) | [Mirror link](https://wallabag.open-web.fr/share/601fed151e81a0.70074443)

2020-02-09: [How to have HTTPs on development with Docker, Traefik v2 and mkcert](https://dev.to/nflamel/how-to-have-https-on-development-with-docker-traefik-v2-and-mkcert-2jh3) | [Mirror link](https://wallabag.open-web.fr/share/601feee80225b3.91187279)

## tutos

2020-12-26: [Un environnement de Dev Linux sous Windows 10 avec mutlipass, vagrant, kvm et ansible](https://blog.stephane-robert.info/post/environnement-dev-windows10-multipass-linux-vagrant-hyperv-kvm-libvirt/) | [Mirror link](https://wallabag.open-web.fr/share/601b049f610292.54539241)

2020-08-21: [Un serveur DNS-over-TLS ft. Docker, Traefik, Pi-hole, Unbound](https://wonderfall.space/dns-over-tls-docker/) | [Mirror link](https://wallabag.open-web.fr/share/601ea5bfa77f75.58641034)

2020-08-01: [Comment j&#x27;ai cris un malware ( but ducatif) avec NodeJS en moins d&#x27;une heure ! - Nicolas Brondin-Bernard](https://blog.nicolas.brondin-bernard.com/comment-jai-ecris-un-malware-a-but-educatif-avec-nodejs-en-moins-dune-heure/) | [Mirror link](https://wallabag.open-web.fr/share/601fe934a20349.36629433)

2020-07-23: [Development and Testing of AWS without AWS: Localstack](https://medium.com/expedia-group-tech/development-and-testing-of-aws-without-aws-localstack-ab02f9425c40) | [Mirror link](https://wallabag.open-web.fr/share/601fe9fccbf404.57164714)

2020-06-15: [Redmarrer proprement un systme bloqu REISUB](http://www.dindoun.lautre.net/spip.php?article17) | [Mirror link](https://wallabag.open-web.fr/share/601fea9b0b62d1.13974274)

2020-05-01: [How I back up my servers using restic and Wasabi object storage](https://angristan.xyz/2018/07/backup-servers-using-restic-wasabi-object-storage/) | [Mirror link](https://wallabag.open-web.fr/share/601feb24ec35f4.71363666)

2020-04-28: [Dcouvrir Prometheus et Grafana par l'exemple - Zwindler's Reflection](https://blog.zwindler.fr/2020/04/13/decouvrir-prometheus-et-grafana-par-lexemple/) | [Mirror link](https://wallabag.open-web.fr/share/601feb6d3e3e54.65064353)

2020-02-22: [Deduplicating Data With XFS And Reflinks](http://dashohoxha.fs.al/deduplicating-data-with-xfs-and-reflinks/) | [Mirror link](https://wallabag.open-web.fr/share/601fee2bdefde5.79122733)

2020-02-09: [How to have HTTPs on development with Docker, Traefik v2 and mkcert](https://dev.to/nflamel/how-to-have-https-on-development-with-docker-traefik-v2-and-mkcert-2jh3) | [Mirror link](https://wallabag.open-web.fr/share/601feee80225b3.91187279)

## ubuntu

2020-04-06: [Kubuntu et les drivers propritaires](https://www.dsfc.net/logiciel-libre/linux/ubuntu-linux-logiciel-libre/kubuntu-et-les-drivers-proprietaires/) | [Mirror link](https://wallabag.open-web.fr/share/601febcf5f82d0.67160840)

## wireguard

2020-12-30: [WireGuard: outils et fichiers de configuration](https://blog.flozz.fr/2020/03/22/wireguard-outils-et-fichiers-de-configuration/) | [Mirror link](https://wallabag.open-web.fr/share/601e9ed5b730e3.34406539)

2020-10-12: [Debian 10 set up WireGuard VPN server - nixCraft](https://www.cyberciti.biz/faq/debian-10-set-up-wireguard-vpn-server/) | [Mirror link](https://wallabag.open-web.fr/share/601ea352662621.59087254)

