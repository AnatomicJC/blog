---
title: Installation de Xvfb
author: JC
date: 2010-07-23T23:35:09+00:00
url: /installation-de-xvfb/
categories:
  - En vrac

---
Xvfb est un serveur X qui peut être lancé sur des machines dépourvues de matériel d&rsquo;affichage ou de carte graphique. Il émule un framebuffer en utilisant la mémoire virtuelle. Xvfb est donc un moyen discret pour exécuter des applications qui n&rsquo;ont pas vraiment besoin d&rsquo;un serveur X, mais insistent pour avoir un.

Nous allons l’installer tout simplement depuis les dépôts :

```
aptitude install xvfb
```

## Démarrage de Xvfb

Pour démarrer Xvfb, c’est très simple, il vous suffit d’éxécuter cette commande:

```
Xvfb :1 -screen 0 1024x768x24
```

Ainsi, Xvfb est démarré:

  * `:1` – Sur l’écran virtuel numéro 1
  * `-screen 0` Avec un indice X à 0
  * `1024x768x24` – Une résolution de 1024×768 avec 24 de color depth

C’est aussi simple. Néanmoins, il faut savoir que la plupart des applications utilisant un serveur X font appelle à la variable `DISPLAY`. Elle contient le numéro d’écran ainsi que l’indice X sous la forme: `[numéro  écran]:[indice X]`. Il vous faut donc exporter cette variable pour qu’elle soit utilisée par ces programmes :

```
export DISPLAY = "1:0"
```

**Note:** Cette commande sera à exécuter à chaque fois que vous aurez une erreur du type « Impossible de se connecter au serveur X »

## Pour que Xvfb soit démarré à chaque fois…

Pour que Xvfb soit démarré à chaque démarrage, il faut le mettre en tant que « service ». Pour cela, créez un petit script de démarrage :

```
#!/bin/sh

case $1 in
	start)
		Xvfb :1 -screen 0 1024x768x24 &;
	;;
	stop)
		killall Xvfb
	;;
	*)
	echo "Usage: $0 (start|stop)"
	exit 1
	;;
esac

exit 0
```

Placez-le dans /etc/init.d/. Puis, nous allons lui ajouter les droits d’éxecution puis le faire démarrer par defaut :

```
chmod +x /etc/init.d/xvfb
update-rc.d xvfb defaults
```
Si votre script est correct, Xvfb devrait démarrer à chaque fois.

_source: <http://www.d-sites.com/2009/09/05/debian-installer-un-serveur-x/>_
