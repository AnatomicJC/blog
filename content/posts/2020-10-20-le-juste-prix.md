---
title: Le juste prix
description: Le juste prix
author: JC
date: 2020-10-20T05:20:02+00:00
url: /le-juste-prix/
categories:
  - En vrac

---
Un client m&rsquo;a demandé combien ça coûtait de faire du carrelage

1500 €  
Si cher pour ce boulot ?  
Combien pensez vous que ça vous coûterait ?  
800 € maximum&#8230;

-Pour 800 € je vous invite à le faire vous-même.  
-Mais&#8230;. je ne sais pas le faire.  
-Pour 800 € je vous apprendrais à le faire. Donc, vous économisez 700 €,  
-il accepte  
-Mais pour commencer : vous avez besoin d&rsquo;outils .  
-Mais je n&rsquo;ai pas tout cela  
-Pour 250€ de plus je vous loue mes affaires pour que vous puissiez le faire.  
-D&rsquo;accord ,  
&#8211; Mardi, je vous attends  
-Mais je ne peux pas le mardi  
-Je suis désolé, Les autres jours sont occupés avec les autres clients.  
-D&rsquo;accord ! Ça veut dire que je vais devoir sacrifier mon mardi.  
-J&rsquo;oubliais. Pour faire vous-même votre travail, avez vous pensé a prendre en compte la fiscalité , tva, sécurité, assurance, carburant?  
-Mais pour remplir ces tâches, je vais dépenser plus d&rsquo;argent et perdre beaucoup de temps !  
-Je vais vous préparer tout le matériel qu&rsquo;il vous faudra . Le chargement du camion se fera mardi matin à 6h30 pour éviter les bouchons  
&#8211; À 6 h30 ? Non !Trop tôt pour moi !  
-J&rsquo;ai réfléchi. Je préfère vous payer les 1500 euros.

Lorsque vous payez un artisan vous payez une multitude de choses !

Personne ne peut dénigrer le travail des autres en jugeant les prix

<https://www.linkedin.com/posts/kevin-cappiot-3972b786_un-client-ma-demand%C3%A9-combien-%C3%A7a-co%C3%BBtait-activity-6723129549481238528-Z17K/>
