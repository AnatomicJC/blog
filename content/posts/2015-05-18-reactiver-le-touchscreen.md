---
title: Réactiver le touchscreen
description: Réactiver le touchscreen sur Gnome 3
author: JC
date: 2015-05-18T08:36:56+00:00
url: /reactiver-le-touchscreen/
featured_image: /wp-content/uploads/2015/05/touch-333x270.jpg
categories:
  - En vrac

---
Source: Merci à [MaTachi du forum ArchLinux][1]

Ayant récemment fait l&rsquo;aquisition d&rsquo;un PC avec touchscreen, j&rsquo;ai remarqué que ce dernier se désactivait après retour de la mise en veille. J&rsquo;utilise GNOME 3.

Pour corriger le souci:

### Créer un script qui recharge le module hid_multitouch.

    $ sudo mkdir /opt/touchscreen-fix
    $ cd /opt/touchscreen-fix
    $ echo '#!/bin/sh
    /sbin/rmmod hid_multitouch && /sbin/modprobe hid_multitouch' | sudo tee resume.sh
    $ sudo chmod +x resume.sh
    

Lancer ce script réactivera votre touchscreen. Bien entendu, il ne va pas s&rsquo;executer automatiquement après chaque retour de veille :-/ C&rsquo;est ce dont on va s&rsquo;occuper ci-dessous.

### Faire en sorte que le touchscreen se réactive après chaque retour de veille.

Créer ce service systemd:

    $ echo '[Unit]
    Description=Fix touchscreen after resume
    After=suspend.target
    
    [Service]
    Type=simple
    ExecStart=/opt/touchscreen-fix/resume.sh
    
    [Install]
    WantedBy=suspend.target' | sudo tee /etc/systemd/system/touchscreen-
    fix.service
    

Et activez-le:

    $ sudo systemctl daemon-reload
    $ sudo systemctl enable touchscreen-fix

 [1]: https://bbs.archlinux.org/viewtopic.php?pid=1484947#p1484947
