---
title: Monter une image raw
author: JC
date: 2010-07-21T14:05:19+00:00
url: /monter-une-image-raw/
categories:
  - En vrac

---
source : http://linuxfr.org/forums/47/25088.html
  
Après avoir sauvegardé l&rsquo;ensemble d&rsquo;un disque avec la commande dd (dd if=/dev/sda of=image.raw), il peut être utile de monter une partition du disque en loppback.
  
Pour cela il faut ajouter l&rsquo;option offset à l&rsquo;option loop de la commande mount. Le numéro de l&rsquo;offset se calcule en multipliant le premier secteur de la partition par le nombre d&rsquo;unités.
  
ex :
  
```
# fdisk -l -u -C 592 image.raw
```
  
```
Disk image.raw: 0 MB, 0 bytes
255 heads, 63 sectors/track, 592 cylinders, total 0 sectors
Units = sectors of 1 * 512 = 512 bytes
Device Boot Start End Blocks Id System
image.raw1 *63 7438094 3719016 7 Linux
```
  
l&rsquo;offset sera egal à 63*512 = 32256
  
Il ne reste plus qu&rsquo;à faire :
  
```
# mount -o loop,offset=32256 -t ext3 image.raw /mnt
```
