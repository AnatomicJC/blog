---
title: "Red Hat docker images"
author: JC
date: 2022-08-07T00:00:01+00:00
url: /how-to-register-redhat-docker-images-to-be-able-to-install-packages
description: How to register RedHat docker images to be able to install packages
cover:
  image: /images/redhat/redhat-logo.svg
tags:
  - redhat
  - memo
  - linux
  - setup
  - docker

ShowToc: true
TocOpen: true
---

## The problem

So you are trying to use [RedHat official docker images, also called UBI for Universal Base Images](https://developers.redhat.com/products/rhel/ubi) in your CI/CD and try to install some packages.

Sometimes it works:

```bash
$ docker run -it --rm redhat/ubi9:latest
[root@6afe7431c74f /]# dnf install nginx
Updating Subscription Management repositories.
Unable to read consumer identity
Subscription Manager is operating in container mode.

This system is not registered with an entitlement server. You can use subscription-manager to register.

Last metadata expiration check: 0:20:09 ago on Sun Aug  7 16:14:04 2022.
Dependencies resolved.
==================================================================================================================================================================================
 Package                                        Architecture                       Version                                      Repository                                   Size
==================================================================================================================================================================================
Installing:
 nginx                                          x86_64                             1:1.20.1-10.el9                              ubi-9-appstream                             607 k
Installing dependencies:
 nginx-filesystem                               noarch                             1:1.20.1-10.el9                              ubi-9-appstream                              13 k
 redhat-logos-httpd                             noarch                             90.4-1.el9                                   ubi-9-appstream                              18 k
Installing weak dependencies:
 logrotate                                      x86_64                             3.18.0-5.el9                                 ubi-9-baseos                                 80 k

Transaction Summary
==================================================================================================================================================================================
Install  4 Packages

Total download size: 718 k
Installed size: 1.9 M
Is this ok [y/N]:
etc etc.
```

Sometimes not :-(

```bash
$ docker run -it --rm redhat/ubi9:latest
[root@6afe7431c74f /]# dnf install selinux-policy-devel
Updating Subscription Management repositories.
Unable to read consumer identity
Subscription Manager is operating in container mode.

This system is not registered with an entitlement server. You can use subscription-manager to register.

Last metadata expiration check: 0:23:31 ago on Sun Aug  7 16:14:04 2022.
No match for argument: selinux-policy-devel
Error: Unable to find a match: selinux-policy-devel
```

In a [previous article](/how-to-install-packages-on-red-hat/), I explained how to get a developer subscription to be able to install packages with `subscription-manager`. But no luck:

```bash
# subscription-manager register --username your-redhat-login
subscription-manager is disabled when running inside a container. Please refer to your host system for subscription management.
```

WTF !!! I cannot register a docker container :-(

To be short, if you are using Docker or Podman on RedHat, **your subscription will be automatically mounted as a secret in /run/secrets folder of your container**.

Upstream Docker has rejected patches that allow for host subscription data to be directly attached to the containers automatically, so if you are trying to use the RedHat UBI images on another system than RedHat, like me, you cannot use `subscription-manager` to register your system.

## The solution

Thankfully, a [colleague of mine](https://www.linkedin.com/in/geroldmougenel/) pointed me to this [RedHat KB](https://access.redhat.com/solutions/5870841) who provides a solution \0/

From a working RedHat OS where you activated your developer subscription (cf [my other blog post](/how-to-install-packages-on-red-hat/)), grab these files:

* /etc/pki/entitlement/ (folder)
* /etc/rhsm (folder)
* /etc/yum.repos.d/redhat.repo (plain file)

And mount them while creating your container:

```bash
docker run -it --rm \
  -v "$PWD/entitlement:/run/secrets/etc-pki-entitlement" \
  -v "$PWD/rhsm:/run/secrets/rhsm" \
  -v "$PWD/redhat.repo:/run/secrets/redhat.repo" \
  redhat/ubi9:latest
```

That's it, you can know install any package :-)
