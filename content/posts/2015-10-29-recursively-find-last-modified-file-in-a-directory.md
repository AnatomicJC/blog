---
title: Recursively find last modified file in a directory
author: JC
date: 2015-10-29T14:45:52+00:00
url: /recursively-find-last-modified-file-in-a-directory/
featured_image: /wp-content/uploads/2015/10/Lost-HQ-Wallpaper-lost-10787813-1920-1080-825x510.jpg
categories:
  - En vrac

---
I need to find the most recent file recursively. Linux find command took around 50 minutes.

Here is a little script to do it faster (less than 2 seconds):

```
#!/bin/sh
zob () {
    if [ -z ${CURRENT_DIR} ]; then
      CURRENT_DIR="$1"
    fi
    FILE=$(ls -Art1 ${CURRENT_DIR} | tail -n 1)
    if [ ! -f ${FILE} ]; then
        CURRENT_DIR="${CURRENT_DIR}/${FILE}"
        zob
    fi
    echo $FILE
    exit
}
zob $1
```

It&rsquo;s a recursive function who get the most recent modified item of a directory. If this item is a directory, the function is called recursively and search into this directory, etc.

To search last modified file in /var/log, just call the script like this:

```
sh script.sh /var/log
```
