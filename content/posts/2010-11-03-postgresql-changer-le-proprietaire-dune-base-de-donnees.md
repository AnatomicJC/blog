---
title: "PostgreSQL: Changer le propriétaire d'une base de données"
author: JC
date: 2010-11-03T09:32:50+00:00
url: /postgresql-changer-le-proprietaire-dune-base-de-donnees/
tags:
  - postgresql
  - Memos

---
Après avoir importé une base de données PostgreSQL de cette manière&#8230;

```
psql nom_de_ma_base < import.sql
```

... OpenERP ne trouvait la trouvait pas. Tout simplement car le propriétaire de la base n'était pas bon.  
Voici comment le changer, on se connecte sous l'utilisateur postgres:

```
sudo su - postgres
```

Puis on lance la requète suivante (adaptez l'utilisateur et le nom de la base):

```
UPDATE pg_database SET datdba = (SELECT usesysid FROM pg_shadow WHERE usename = 'nom_de_l_utilisateur') WHERE datname = 'nom_de_la_base'
```
