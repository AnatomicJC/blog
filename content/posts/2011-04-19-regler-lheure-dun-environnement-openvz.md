---
title: Régler l'heure d'un environnement virtuel OpenVZ
description: Régler l'heure d'un environnement virtuel OpenVZ
author: JC
date: 2011-04-19T08:14:32+00:00
url: /regler-lheure-dun-environnement-openvz/
featured_image: /wp-content/uploads/2011/04/sexy-vps.jpg
tags:
  - openvz
  - Memos

---
Tuto servant à régler l&rsquo;heure d&rsquo;une machine virtuelle OpenVZ. Source: <http://forum.openvz.org/index.php?t=msg&goto=6694&>

Admettons que l&rsquo;environnement virtuel ait l&rsquo;ID 101.

On stoppe le VE:

```
# vzctl stop 101
```

On lui donne la possibilité de régler l&rsquo;heure:

```
# vzctl set 101 --capability sys_time:on --save
```

On le démarre:

```
# vzctl start 101
```

On entre dedans pour finaliser l&rsquo;opération:

```
# vzctl enter 101
```

On définit par exemple le fuseau horaire de Paris:

```
# mv /etc/localtime /etc/localtime.old
# ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

Fini, l&rsquo;environnement virtuel est à l&rsquo;heure !
