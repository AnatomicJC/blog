---
title: Consommation mémoire
author: JC
date: 2010-07-21T13:51:07+00:00
url: /consommation-memoire/
tags:
  - Memos

---
Pour trouver le programme qui consomme le plus de mémoire :

```
$ ps -efF --sort rss
```

Il faut regarder la 6e colonne (rss) et le plus gros consommateur se trouve en bas.
