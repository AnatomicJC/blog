---
title: Convertir des vidéos de Youtube en mp3 / Intégration PCManFM
description: Convertir des vidéos de Youtube en mp3 / Intégration PCManFM
author: JC
date: 2013-10-06T12:42:00+00:00
url: /convertir-des-videos-de-youtube-en-mp3-integration-pcmanfm/
featured_image: /wp-content/uploads/2013/10/1285069-youtube.jpg
categories:
  - En vrac
tags:
  - lxde
  - youtube
  - Memos

---
Bon, ce qu&rsquo;on va faire aujourd&rsquo;hui est dans le titre, convertir des vidéos de Youtube en mp3.

C&rsquo;est tout simple avec le script [youtube-dl][1] présent dans toutes les bonnes distributions.<!--more-->

On installe également ffmpeg pour convertir la vidéo en mp3.

```sh
# apt-get install youtube-dl ffmpeg libavcodec-extra-53
```

On prépare déjà un simple fichier texte qui va contenir les URL des vidéos à downloader (une URL par ligne, c&rsquo;est important). Appelons par exemple ce fichier **youtube-urls.txt:**

```
https://www.youtube.com/watch?v=Aq54hytfFEg
https://www.youtube.com/watch?v=Qy5R7NFA1hc
```

Ensuite, il suffit d&rsquo;appeler ce fichier avec cette ligne de commande:

```
$ youtube-dl -o "%(title)s.%(ext)s" --extract-audio --audio-format=mp3 -a youtube-urls.txt
```

  * -o « %(title)s.%(ext)s »: permet de ne garder que le titre dans le nom du fichier
  * &#8211;extract-audio: ben, on extrait l&rsquo;audio
  * &#8211;audio-format=mp3: on dit que l&rsquo;on veut un mp3
  * -a youtube-urls.txt: on dit à youtube-dl de télécharger les URL présentes dans le fichier youtube-urls.txt

# Intégration dans PCManFM

PCManFM est le gestionnaire de fichiers de LXDE. On va maintenant simplifier les choses en créant un fichier texte, puis lors d&rsquo;un clic droit sur celui-ci, nos vidéos seront automatiquement téléchargées et converties en mp3.

Créez en root un script dans /usr/local/bin/ nommé youtube:

```
# vim /usr/local/bin/youtube
```

```
#!/bin/bash
cd `dirname $@`
youtube-dl -o "%(title)s.%(ext)s" --extract-audio --audio-format=mp3 -a $@
```

Rendez ce script exécutable:

```
# chmod +x /usr/bin/youtube
```

Ensuite:

  1. Créez un dossier
  2. Créez un fichier texte dans ce dossier contenant les URL youtube à downloader (une par ligne, c&rsquo;est important)
  3. Cliquez-droit sur ce fichier => « Ouvrir avec&#8230; »
  4. Cliquez sur l&rsquo;onglet « Ligne de commande personnalisée » et entrez cette commmande:

```
youtube %f
```

Vous n&rsquo;avez plus qu&rsquo;à valider.  
La prochaine fois, vous aurez juste à cliquer droit, et vous aurez dans la liste des applications disponibles votre script youtube.

*Et si je me suis vautré dans la commande? Comment je fais pour éditer?*

Toutes les commandes personnalisées se trouvent dans votre home à ce chemin:

*~/.local/share/applications/*

Vous trouverez dans ce dossier votre commande personnalisée sous la forme userapp-youtube-XXXXXX.desktop

Il vous suffit de l&rsquo;éditer ou la supprimer.

 [1]: http://rg3.github.io/youtube-dl/
