---
title: BackupPC Howto
description: backuppc howto 
author: JC
date: 2021-01-27T18:10:02+00:00
url: /backuppc-howto/
tags:
  - Tutos
  - BackupPC

ShowToc: true
TocOpen: true

---
BackupPC
========

Installation
---------

BackupPC is a backup software written in perl. It is packaged on all modern Linux distros, to install it on Debian family:

    # apt-get install backuppc

That's all.

How it works ?
===========
BackupPC is a ***set it then forget it*** backup software.
Backups are performed by ***backuppc*** user through ***ssh*** and ***rsync***

BackupPC doesn't perform backups at fixed hours, but checks periodically with ***ping*** command if a host is alive or not. Then, it checks if there is recent backup of this host.

Default backup policy:

 - Full backups every 7 days (not a weekly backup every sunday, each 7 days !!!)
 - Incremental backups once a day
 - Backup are not performed during "blackouts period" (between 7am to 7pm30 from Monday to Friday)
 - Default settings can be overwritten in config section of each host

If there is recents backups, BackupPC will do nothing if we are in blackout period. But if a backup is older than 7 days, BackupPC will try to backup it, even in blackout period, by all means.

Good to know
==

BackupPC works with Hostnames, not IPs
--

BackupPC doesn't work with IP, only with FQDN. So you must have a DNS who resolves your hosts you want to backup.

Poor-man-method is /etc/hosts file.

SSH configuration
--

BackupPC can backup Samba or FTP shares, but SSH is preferred, as we will backup only Linux servers.

`backuppc` user ***must be able to connect to the host without password***. So create a ssh key and copy the public keys to the `~/.ssh/authorized_keys` file.

To test if backuppc will be able to connect to the host, connect as backuppc user on the server then try to connect to the host.

BackupPC must access to *ALL* files
--

BackupPC tries to connect to host by default with root user. As it is not a good practice, it is better to create a `backuppc` system user on the host to backup:

    # useradd backuppc --system -m

Then configure `sudo` for this user to be able to backup root files.

Be careful because as explained [here](http://backuppc.sourceforge.net/faq/ssh.html#how_can_client_access_as_root_be_avoided):

> Note that an attacker who can become the backuppc user on the server
> can then ssh to the client and use sudo to run `/bin/tar` as root.
> Importantly, this means they can write files anywhere on the file
> system by doing `tar -x` or using `rsync` to copy files. This is
> sufficient to allow an attacker to become root on the client machine.

Launch `visudo`:

     backuppc  ALL=NOPASSWD: /usr/bin/rsync

And for *RsyncClientCmd* and *RsyncClientRestoreCmd* directive, replace `root` with `backuppc` and add sudo.

    $sshPath -t -q -x -l backuppc $host nice -n 19 sudo $rsyncPath $argList+

Be sure you can log in with backuppc user only from IP

Another tips:

 * restrict login to backuppc user through ssh with the `AllowUser` directive in `/etc/ssh/sshd_config` file.
 * before restart ssh service, always check if the config is OK with the `sshd -t` command, and check you can always log in by test a new SSH connection in another terminal

ping configuration
==

BackupPC checks if a host is alive or not with ping command. If ping is disabled on your servers, you can override it by a script who will return 0 if connection is successful. We can check if a server is alive or not with SSH.

Example script:

    #!/bin/sh
    if [ "$1" = "localhost" ]; then
      return 0
    fi
    ssh -q backuppc@$1 exit

In /etc/backuppc/config.pl, override the PingCmd and PingPath commands:

    $Conf{PingPath} = '/path/to/your/script';
    $Conf{PingCmd} = '$pingPath $host';

`Ping` test through SSH gives some latency. If a host doesn't respond in less than 20 ms, BackupPC considers host is not reachable, and you can have this kind of message in log file:

    2015-12-07 17:18:52 ping too slow: 20.17msec
    2015-12-07 17:21:49 ping too slow: 20.75msec
    2015-12-07 17:46:18 ping too slow: 20.3msec
    2015-12-07 18:00:01 ping too slow: 20.3msec

So increase the PingMaxMsec directive to fix this:

    $Conf{PingMaxMsec} = 60;

Linux server backup configuration
==

You can backup whole `/` (=slash aka root path in Linux). Enter `/` for RsyncShareName. You will have to exclude some useless directories such as `/proc`, `/dev`, `/sys` in the `BackupFilesExclude` section.

> If you are using BackupPC interface, use `*` as **new key**. More details [here](http://backuppc.sourceforge.net/faq/BackupPC.html#_conf_backupfilesexclude_)

Path of BackupPC perl scripts
==

You can find BackupPC perl scripts here: `/usr/share/backuppc/bin/`

    root@backup: ~ # ls /usr/share/backuppc/bin/
    BackupPC      BackupPC_archiveHost   BackupPC_attribPrint  BackupPC_fixupBackupSummary  BackupPC_nightly  BackupPC_sendEmail   BackupPC_tarCreate   BackupPC_tarPCCopy   BackupPC_zcat
    BackupPC_archive  BackupPC_archiveStart  BackupPC_dump             BackupPC_link                BackupPC_restore  BackupPC_serverMesg  BackupPC_tarExtract  BackupPC_trashClean  BackupPC_zipCreate

Host removal
==

Keep an archive of the host
---

`/usr/share/backuppc/bin/BackupPC_archiveHost tarCreatePath splitPath parPath host bkupNum compPath fileExt splitSize outLoc parFile share`

It can sounds complex, here is an explanation:

* tarCreatePath: /usr/share/backuppc/bin/BackupPC_tarCreate
* splitPath: split command (can be ignored => /bin/false)
* parPath: par command (can be ignored => /bin/false)
* host: host name, you can find it on /etc/backuppc/hosts
* bkupNum: backup number
* comPath: compression command, you can use gzip, but pigz is a better alternative => /usr/bin/pigz
* fileExt: extension => .gz
* splitSize: we doesn't split => 0
* outLoc: output path of the archive, you can write a dot for current path => .
* parFile: par file, a dot for current file => .
* share name: check path in BackupPC web interface, don't forget "/" in begin and end. If you want all files => `\*`

Here is an example if you want an archive of **backup 57** of **asterix** host:

    /usr/share/backuppc/bin/BackupPC_archiveHost /usr/share/backuppc/bin/BackupPC_tarCreate /bin/false /bin/false asterix 57 /usr/bin/pigz  .gz 0 . . \*

Remove the host
---

To remove a machine from backups, 3 steps:

* Remove host's configuration file :
  `rm /etc/backuppc/host.pl`
* Delete host occurence in ''/etc/backuppc/hosts'' then reload backuppc service :
  `/etc/init.d/backuppc reload`
* Move host's backups files to ''/var/lib/backuppc/trash'' :
  `mv /var/lib/backuppc/pc/host /var/lib/backuppc/trash/`

Misc
==

BackupPC Full documentation
--

* On your BackupPC server: http://backup/backuppc/index.cgi?action=view&type=docs
* On sourceforge: http://backuppc.sourceforge.net/faq/BackupPC.html

BackupPC daily report
--

A daily report of the summary page can be sent, here is the script `/root/backuppc-report/report.sh`

    #!/bin/sh

    BASEDIR="$( cd "$( dirname "$0" )" && pwd )"
    TMP_FILE="${BASEDIR}/backuppc_report_tmp_file"
    HTUSER="backuppc"
    HT_PASSWD="Put here password of your backuppc user"
    URL="http://${HTUSER}:${HTPASSWD}@localhost/backuppc/index.cgi?action=summary"
    SENDER="admin@example.com"
    RCPT="admin@example.com"
    TITLE="BackupPC Report"

    wget -O ${TMP_FILE} --quiet --no-check-certificate ${URL}

    sed -i 's!\(<td.*10%.*\)>!\1 style="display: none;" >!' ${TMP_FILE}
    sed -i 's!<head>!<head><style type="text/css">table {font-size: 12px;}</style>!' ${TMP_FILE}
    sed -i 's!90%!100%!' ${TMP_FILE}
    sed -i 's!^.*http://backuppc.sourceforge.net.*img src.*$!!' ${TMP_FILE}
    sed -i 's!\(<div class="NavMenu" id="NavMenu" style="\)height:100%">!\1display: none;"!' ${TMP_FILE}

    cat ${TMP_FILE} | mail -s "$TITLE" -a "Content-type: text/html" -a "From: $SENDER" $RCPT

    rm -f ${TMP_FILE}

In `/etc/crontab`:

    0 10 * * *      root    /root/backuppc-report/report.sh

Move BackupPC to another machine
==

On the new machine, install backuppc and stop backuppc service on both old and new backuppc server.
Transfer /etc/backuppc config files and backuppc .ssh configs.

From the old server, you have to transfer the whole /var/lib/backuppc/cpool/ directory who contains all backups.
Once it is done, as backuppc user, you have to launch the BackupPC_tarPCCopy tool to copy BackupPC hardlinks in /var/lib/backuppc/pc folder.

It works like this:

    /usr/share/backuppc/bin/BackupPC_tarPCCopy /var/lib/old_backuppc_folder/pc/ | tar xPf - -C /var/lib/new_backuppc_folder/pc/

To migrate backuppc pc folder from one server to another machine through SSH, here is the command line:

    su -c "/usr/share/backuppc/bin/BackupPC_tarPCCopy /var/lib/backuppc/pc/" -s /bin/bash backuppc | ssh iuser@another-machine tar xPf - -C /var/lib/backuppc/pc/
