---
title: "GPG Memo for passbolt"
author: JC
date: 2022-07-11T00:00:01+00:00
url: /gpg-memo-passbolt
description: Silently create RSA and Elliptic ed25519 GPG keys
tags:
  - gpg
  - memo
  - passbolt

ShowToc: true
TocOpen: true
---

I use for this memo a temporary gpg homedir named `gpg-tmp`

```bash
mkdir gpg-tmp
```

Since 3.6.0 version, passbolt can use Elliptic ed25519 GPG keys.

We will see how to silently create both RSA/ECC GPG keys

## Create RSA key with passphrase

```bash
gpg --homedir gpg-tmp --batch --no-tty --gen-key <<EOF
    Key-Type: rsa
    Key-Length: 4096
    Key-Usage: sign,cert
    Subkey-Type: rsa
    Subkey-Length: 4096
    SubKey-Usage: encrypt
    Name-Real: John Doe
    Name-Email: john@doe.com
    Expire-Date: 0
    Passphrase: a-strong-passphrase
    %commit
EOF
```

## Create RSA key without passphrase

```bash
gpg --homedir gpg-tmp --batch --no-tty --gen-key <<EOF
    Key-Type: rsa
    Key-Length: 4096
    Key-Usage: sign,cert
    Subkey-Type: rsa
    Subkey-Length: 4096
    SubKey-Usage: encrypt
    Name-Real: John Doe
    Name-Email: john@doe.com
    Expire-Date: 0
    %no-protection
    %commit
EOF
```

## Create ECC key with passphrase

```bash
gpg --homedir gpg-tmp --batch --no-tty --gen-key <<EOF
    Key-Type: eddsa
    Key-Curve: ed25519
    Key-Usage: sign,cert
    Subkey-Type: ecdh
    Subkey-Curve: cv25519
    SubKey-Usage: encrypt
    Name-Real: John Doe
    Name-Email: john@doe.com
    Expire-Date: 0
    Passphrase: a-strong-passphrase
    %commit
EOF
```

## Create ECC key without passphrase

```bash
gpg --homedir gpg-tmp --batch --no-tty --gen-key <<EOF
    Key-Type: eddsa
    Key-Curve: ed25519
    Key-Usage: sign,cert
    Subkey-Type: ecdh
    Subkey-Curve: cv25519
    SubKey-Usage: encrypt
    Name-Real: John Doe
    Name-Email: john@doe.com
    Expire-Date: 0
    %no-protection
    %commit
EOF
```

## List and export public and secret keys

### List secret keys

```bash
gpg --homedir gpg-tmp --list-secret-keys
```

### List public keys

```bash
gpg --homedir gpg-tmp --list-keys
```

### Export secret keys

I assume here you want to export key with fingerprint 9AB9F0A3317DC33382E5DACCCC391CCC349A9FD2

```bash
gpg --armor --homedir gpg-tmp --export-secret-keys 9AB9F0A3317DC33382E5DACCCC391CCC349A9FD2
```

### Export public keys

I assume here you want to export key with fingerprint 9AB9F0A3317DC33382E5DACCCC391CCC349A9FD2

```bash
gpg --armor --homedir gpg-tmp --export 9AB9F0A3317DC33382E5DACCCC391CCC349A9FD2
```
