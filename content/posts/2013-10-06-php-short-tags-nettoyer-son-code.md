---
title: 'PHP Short tags: Nettoyer son code'
description: 'PHP Short tags: Nettoyer son code'
author: JC
date: 2013-10-06T13:15:18+00:00
url: /php-short-tags-nettoyer-son-code/
featured_image: /wp-content/uploads/2013/10/45527_un_petit_coup_de_karcher.jpeg
categories:
  - En vrac
tags:
  - PHP

---
Vous ne savez pas ce que sont les shorts tags en PHP, et pourquoi il faut les virer ?

=> http://www.apprendre-php.com/tutoriels/tutoriel-3-pourquoi-il-est-dconseill-d-utiliser-les-balises-courtes-short-tags.html

Voici maintenant un petit script qui vous permettra de nettoyer votre code de ces affreux short tags:

Cette première boucle va chercher dans votre code tout ce qui commence par `<?` et le remplacer par `<?php` (`<?php`, `<?=` et `<?xml` ne seront pas impactés):


```
for file in \`find -name *php\`; do perl -pi -e 's#<\?(?!(php|=|xml))#<?php#g' $file; done
```

Cette seconde boucle va chercher toutes les occurences de <?= et les remplacer par <?php echo:


```
for file in \`find -name *php\`; do perl -pi -e 's#<\?=#<?php echo #g' $file; done
```

Voilà, vous avez un code tout propre.
