---
title: L'homme pressé refait deux fois la même chose
description: prendre du recul sur les situations
author: JC
date: 2021-07-27T05:00:00+00:00
url: /l-homme-presse-refait-deux-fois-la-meme-chose/
tags:
  - Coaching

---

> Le site où se trouvait le post qui suit n'existe plus. On peut en trouver [une archive ici](http://web.archive.org/web/20180412131501/http://www.bouzin-agile.fr/?post/2013/05/27/L-homme-presse-refait-deux-fois-la-meme-chose).

Vous avez un objectif, un truc qui semble simple, tellement simple que vous foncez, vous foncez et BOOM, un obstacle.

Regardez la suite en 4 images, ça fait mal mais c'est tellement vrai...

### Acte 1 : Vous regardez votre objectif

![](/images/l-homme-presse-refait-deux-fois-la-meme-chose/kaizen2.jpg)

### Acte 2 : Vous essayez d'atteindre votre objectif

![](/images/l-homme-presse-refait-deux-fois-la-meme-chose/kaizen1.jpg)

### Acte 3 : En bon soldat, vous y retournez tête baissée

![](/images/l-homme-presse-refait-deux-fois-la-meme-chose/kaizen3.jpg)

### Acte 4 : Vu de loin, on ne comprend pas trop ce que vous faites

![](/images/l-homme-presse-refait-deux-fois-la-meme-chose/kaizen4.jpg)

### Acte 5 : La morale

Cette situation est un grand classique :

* "On" vous demande de courir
* Vous n'avez pas 1 seconde pour analyser comment vous travaillez
* Vos collègues ont aussi la tête dans le guidon
* Et votre manager est tellement occupé qu'il ne se rend pas compte de la situation


Alors la prochaine fois que vous êtes bloqués sur quelque chose, au lieu d'essayer de passer en force, prenez du recul et demandez vous ce que vous voulez faire, généralement vous allez gagner énormément de temps et d'énergie.

**Message de service aux managers :**

Aidez vos collaborateurs en difficulté à prendre du recul, mais bien sur pour cela il faut venir voir ce qu'il se passe dans vos équipes...

N'oubliez pas :

* « Les batailles se perdent dans la précipitation. »
* « Soyez rapide sans vous presser. »
* « Les gens heureux n’ont pas besoin de se presser. »
* « Il n'y a rien d'urgent, il n'y a que des gens pressés. »

et

* « L’homme pressé refait deux fois la même chose. »
