---
title: Changer le navigateur par défaut avec update-alternatives
author: JC
date: 2010-07-21T12:50:14+00:00
url: /changer-le-navigateur-par-defaut-avec-update-alternatives/
tags:
  - Memos

---
Source : [Coagul](http://www.coagul.org/spip.php?article532)
  
Ayant récemment installé [Iron](http://www.srware.net/en/software_srware_iron.php) comme navigateur, il m&rsquo;a fallu le définir comme navigateur par défaut.
  
Création d&rsquo;une alternative :
  
```
# update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/bin/iron 500
```
  
Sélection d&rsquo;une alternative :
  
```
# update-alternatives --config x-www-browser
```
  
Pour info, suppression de l&rsquo;alternative :
  
```
# update-alternatives --remove x-www-browser /repertoire/de/l/alternative
```
  
Pour vérifier que l’alternative fonctionne :
  
```
$ /etc/alternatives/x-www-browser
```
