---
title: Restrict a given network interface for a particular user using iptables on Linux
description: Restrict a given network interface for a particular user using iptables on Linux
author: JC
date: 2019-09-05T05:25:05+00:00
url: /restrict-a-given-network-interface-for-a-particular-user-using-iptables-on-linux/
categories:
  - En vrac

---
I assume that users A and B are using the same Linux machine(s) where you are the administrator. 

The following command will prevent the user with uid 1234 from sending packets on the interface `eth0`:

``` bash
# iptables -t mangle -A OUTPUT -o eth0 -m owner --uid-owner 1234 -j DROP
```

Thanks to this [StackExchange thread.][1]

 [1]: https://unix.stackexchange.com/questions/21650/how-to-restrict-internet-access-for-a-particular-user-on-the-lan-using-iptables
