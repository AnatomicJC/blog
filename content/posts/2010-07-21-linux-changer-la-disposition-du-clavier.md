---
title: 'Linux : Changer la disposition du clavier'
description: 'Linux : Changer la disposition du clavier'
author: JC
date: 2010-07-21T13:49:57+00:00
url: /linux-changer-la-disposition-du-clavier/
tags:
  - Memos
  - Linux

---
## Depuis l&rsquo;interface graphique

Si vous êtes logué sous X (Gnome, KDE, etc.), vous pouvez changer le mapping du clavier avec <strong>setxkbmap</strong>. Par exemple :
  
Pour un clavier fr :
  
```
setxkbmap fr
```
  
Pour un clavier us :
  
```
setxkbmap us
```

## Depuis la console

En console, la comande `setxkbmap` ne fonctionne pas, il faut utiliser à la place `loadkeys` comme suit :
  
```
loadkeys /emplacement/du/codage/clavier
```
  
Pour connaitre l&#8217;emplacement du codage clavier à charger, il faut regarder dans le manuel :
  
```
man loadkeys
```
  
Par exemple chez Debian pour un clavier suisse :
  
```
# loadkeys /usr/share/keymaps/i386/qwertz/fr_CH.kmap.gz
```
  
Au prochain reboot, les modifications seront réinitialisées.
  
Pour modifier le codage clavier définitivement chez Debian :
  
```
# dpkg-reconfigure console-data
```
  
Pour les autres distributions, Google est votre pote :-)
