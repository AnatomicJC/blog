---
title: 'Extensions PHP PECL: créer un paquet Debian'
author: JC
date: 2010-11-16T15:03:30+00:00
url: /extensions-php-pecl-creer-un-paquet-debian/
categories:
  - En vrac
tags:
  - Debian

---
En consultant dernièrement les Status Reports dans Drupal, ce dernier me conseillait d&rsquo;installer l&rsquo;extension PECL uploadprogress afin d&rsquo;avoir une barre de progression pour l&rsquo;upload de fichiers, bref&#8230;  
Si on consulte la [doc PHP][1], on peut voir qu&rsquo;il suffit de faire un simple:

```
$ pecl install extension
```

Nous allons voir ici comment créer un paquet Debian de cette extension PECL. Pourquoi un paquet Debian ? Pour que notre petite extension ajoutée à la mimine soit connue du système apt.

On installe les paquets nécessaires:

```
# aptitude install php5-dev dh-make-php fakeroot libicu-dev xsltproc
```

C&rsquo;est le paquet dh-make-php qui va nous permettre de construire les sources nécessaires à notre futur paquet Debian.

Une petite description:

```
$ apt-cache show dh-make-php
(...)
Description: Creates Debian source packages for PHP PEAR and PECL extensions
 Contains dh-make-pear and dh-make-pecl, which create Debian source packages
 from PHP PEAR and PECL packages respectively.
(...)
```

Une fois les paquets installés, on peut remplacer _pecl install extension_ par :

```
$ dh-make-pecl --only 5 --maintainer "John Doe <jdoe@example.com>" extension
```

Remplacez John Doe par votre nom et adresse mail, et bien sur « extension » par l&rsquo;extension PECL que vous voulez installer. dh-make-pecl se charge de downloader et préparer le répertoire source pour vous.  
L&rsquo;option &#8211;only 5, c&rsquo;est pour ne prendre en compte que php5.

Vous devriez maintenant avoir un répertoire portant le nom de votre extension. Placez-vous dedans et faites pour contruire le paquet:

```
$ dpkg-buildpackage -rfakeroot
```

Retournez dans le répertoire parent, vous devriez y trouver un paquet *.deb tout frais.

Il n&rsquo;y a plus qu&rsquo;à installer !!

 [1]: http://www.php.net/manual/fr/install.pecl.pear.php "doc PHP"
