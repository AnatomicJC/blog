---
title: Fix broken dependencies of a Debian package
description: Fix broken dependencies of a Debian package
author: JC
date: 2019-06-19T08:55:13+00:00
url: /fix-broken-dependencies-of-a-debian-package/
categories:
  - En vrac

---
Today a quick how-to about repackaging of a Debian package with broken dependencies.

## The context

I use a package automatically built from a custom repo who is broken. It uses a mandatory broken dependency `libssl1.0.0.` This outdated package has been retired from Debian Stretch repositories.

As this custom repository don&rsquo;t provide package sources, I had to dig into this debian package to edit the `control` file, where package dependencies are defined.

## Let&rsquo;s go

You can change the dependencies of a deb package like this:

  1. Unpack deb:&nbsp;`ar x golden-linux.deb`&nbsp;(will create i.e. three files: debian-binary control.tar.gz data.tar.gz)
  2. Unpack control archive:&nbsp;`tar xzf control.tar.gz`&nbsp;(will create: postinst postrm preinst prerm md5sums control)
  3. Fix dependencies in&nbsp;`control`&nbsp;(use a text editor)
  4. Repack control.tar.gz:&nbsp;`tar --ignore-failed-read -cvzf control.tar.gz {post,pre}{inst,rm} md5sums control`
  5. Repack deb:&nbsp;`ar rcs newpackage.deb debian-binary control.tar.gz data.tar.gz`&nbsp;(order important! See [Note] )

[Note]: dpkg wouldn&rsquo;t be able to read the metadata of a package quickly if it had to search for where the data section ended!

So, I was able to replace the `libssl1.0.0` broken dependency with the `libssl1.0.2` one, who is part of Debian stretch.

Thanks to <https://serverfault.com/questions/250224/how-do-i-get-apt-get-to-ignore-some-dependencies> and my Google search ability who point me to the solution.
