---
title: Memo tmux config
description: Memo tmux config
author: JC
date: 2021-11-27T10:00:00+00:00
url: /memo-tmux-config/
tags:
  - Tmux
  - Config

#ShowToc: true
#TocOpen: true
---

Here is my tmux config, works on both MacOS / Linux

```
#https://gitlab.com/AnatomicJC/tmux-themepack
source "${HOME}/git/github/tmux-themepack/powerline/default/blue.tmuxtheme"
set -g default-terminal "screen-256color"
set -g mouse on
set -g history-limit 500000

bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"

# https://blog.jpalardy.com/posts/tmux-synchronized-panes/
bind C-x setw synchronize-panes

# Memos
# tmux list-keys | grep selection
# tmux show -wg mode-keys (must return vi, not emacs)

# For binding 'y' to copy and exiting selection mode
#bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'pbcopy'
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
```
