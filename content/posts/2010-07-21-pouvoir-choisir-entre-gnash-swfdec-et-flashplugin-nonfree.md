---
title: Pouvoir choisir entre Gnash, Swfdec et Flashplugin-nonfree
author: JC
date: 2010-07-21T14:07:18+00:00
url: /pouvoir-choisir-entre-gnash-swfdec-et-flashplugin-nonfree/
tags:
  - Memos
  - update-alternatives

---
Lorsque l&rsquo;on a plusieurs plugins d&rsquo;installés pour lire les flashs tel gnash, swfdec ou flash-nonfree, il est possible de dire à Debian lequel on préfère :
  
```
# update-alternatives --config flash-mozilla.so
```
  
Qui va répondre :
  
```
Il y a 3 alternatives fournissant « flash-mozilla.so ».

  Sélection    Alternative
-----------------------------------------------
          1    /usr/lib/gnash/libgnashplugin.so
          2    /usr/lib/swfdec-mozilla/libswfdecmozilla.so
*+        3    /usr/lib/flashplugin-nonfree/libflashplayer.so

Appuyez sur Entrée pour conserver la valeur par défaut[*] ou choisissez le numéro sélectionné :
```
