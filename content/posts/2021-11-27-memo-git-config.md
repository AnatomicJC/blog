---
title: Memo git config
description: memo git config cheatsheet
author: JC
date: 2021-11-27T10:00:00+00:00
url: /memo-git-config/
tags:
  - Git
  - Config

#ShowToc: true
#TocOpen: true
---

Everytime I configure a new PC or env, I search for this:

```
git config --global user.name "John Doe"
git config --global user.email "johndoe@domain.tld"
git config --global gpg.program gpg
git config --global pull.ff only
git config --global commit.gpgsign true
git config --global init.defaultBranch main
git config --global push.default current
git config user.email "johndoe@domain.tld"
```
