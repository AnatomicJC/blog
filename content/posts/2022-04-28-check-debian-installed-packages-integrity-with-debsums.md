---
title: Check Debian installed packages integrity with debsums
author: JC
date: 2022-04-28T00:00:00+00:00
url: /check-debian-installed-packages-integrity-with-debsums/
description: Check Debian installed packages integrity with debsums
cover:
  image: /images/debian/debian-logo.svg
tags:
  - Debian
  - Tips

ShowToc: true
TocOpen: true
---

Hey ! You inherited the maintenance of a Debian or Ubuntu server and you wonder if the previous admin has modified some sensitive files ? Is your system safe ? Can you trust it ?

debsums to the rescue !

## TL;DR

```bash
sudo apt install debsums
sudo debsums -as --no-prelink --no-locale-purge
```

## Explain me how it works

As you may know, when you install a Debian package, you can find their postinst, postrm, preinst or prerm scripts on `/var/lib/dpkg/info` folder.

```bash
$ ls -alh /var/lib/dpkg/info/debsums.*
-rw-r--r-- 1 root root  116 Jul 14  2018 debsums.conffiles
-rw-r--r-- 1 root root 1.5K Apr 28 06:47 debsums.list
-rw-r--r-- 1 root root 1.5K Jul 14  2018 debsums.md5sums
-rwxr-xr-x 1 root root 1.3K Jul 14  2018 debsums.postinst
-rwxr-xr-x 1 root root 1.3K Jul 14  2018 debsums.postrm
```

An interesting file is the .md5sums one, who contain a shasum of each file installed by the package, eg. the debsums one:

```bash
$ cat debsums.md5sums
16878447551a73b29cb1395223264659  usr/bin/debsums
e94d7179b008960df541ac9a13143514  usr/bin/rdebsums
6ca0fc7fbb0e9703ea16f4d6d4653a87  usr/sbin/debsums_init
c354050db680f86b47f1cf57fc6c4d5b  usr/share/doc/debsums/README
ab1f1110020d2fafd8e2493f9ec2a16a  usr/share/doc/debsums/changelog.gz
770d751553e6559e9eaefd2e11ccf7e9  usr/share/doc/debsums/copyright
08ee7daec878279ffd34d073c2f93098  usr/share/lintian/overrides/debsums
1563dd58bd1db2afe6b52725a1a883b2  usr/share/man/de/man1/debsums.1.gz
41590f195bd24e233054b67994f961ec  usr/share/man/de/man8/debsums_init.8.gz
b8eff242e9af75a306e495d48d355472  usr/share/man/es/man1/debsums.1.gz
045941a6501e5138aef343ecf9ddfec3  usr/share/man/es/man8/debsums_init.8.gz
d8f4e6212023e4e87dfb92aa8b2d546a  usr/share/man/fr/man1/debsums.1.gz
5c3fd5e2d14592348641396d98991f73  usr/share/man/fr/man8/debsums_init.8.gz
f618cd24ae294c0048ddcadec23b7cb4  usr/share/man/man1/debsums.1.gz
6c762efb36a5d62162ca8757fc9d41c1  usr/share/man/man1/rdebsums.1.gz
624f39cbc100fe4dc006f642342b6c58  usr/share/man/man8/debsums_init.8.gz
7576e1b6392c5aab9d0c0a3672d4d58c  usr/share/man/pt/man1/debsums.1.gz
a1a3c6714228726f32c75c79c9b5a5ee  usr/share/man/pt/man8/debsums_init.8.gz
a08a945573bde383284cb848d3486845  usr/share/man/ru/man1/debsums.1.gz
19e81512c718e2af2f0646cf78186117  usr/share/man/ru/man8/debsums_init.8.gz
08deea301a908578741ed10b1c13a580  usr/share/man/sv/man1/debsums.1.gz
1ddd11b004185df36fce426618798116  usr/share/man/sv/man8/debsums_init.8.gz
```

debsums will analyze each file referenced in all .md5sums files from `/var/lib/dpkg/info` folder and check if there is any modified one \0/

You are now good to go for a Debian package system analysis:

```bash
$ sudo debsums -as --no-prelink --no-locale-purge
debsums: changed file /etc/ssl/openssl.cnf (from openssl package)
debsums: changed file /usr/bin/os-prober (from os-prober package)
```

As you can see, debsums reported 2 files on my system who has been modified. It's worth investigating :)
