---
title: How to disable CPU mitigations (Danger zone)
description: How to disable CPU mitigations (Danger zone)
author: JC
date: 2022-02-23T00:00:00+00:00
url: /how-to-disable-cpu-mitigations/
tags:
  - Spectre / Meltdown flaw
  - Security
  - Linux

ShowToc: true
TocOpen: true
---

## CPU vulnerabilities and mitigations

Yesterday, a co-worker pointed out to me than `lscpu` reported CPU vulnerabilities.

```
$ lscpu
(...)
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Mitigation; PTE Inversion; VMX conditional cache flushes, SMT disabled
Vulnerability Mds:               Mitigation; Clear CPU buffers; SMT Host state unknown
Vulnerability Meltdown:          Mitigation; PTI
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled via prctl and seccomp
Vulnerability Spectre v1:        Mitigation; usercopy/swapgs barriers and __user pointer sanitization
Vulnerability Spectre v2:        Mitigation; Full generic retpoline, IBPB conditional, IBRS_FW, STIBP disabled, RSB filling
Vulnerability Srbds:             Unknown: Dependent on hypervisor status
Vulnerability Tsx async abort:   Not affected
```

No problem, all these vulnerabilities are mitigated by my Linux kernel.

I investigated a bit and these informations come from `/sys/devices/system/cpu/vulnerabilities/`:

```
$ grep . /sys/devices/system/cpu/vulnerabilities/*
/sys/devices/system/cpu/vulnerabilities/itlb_multihit:Not affected
/sys/devices/system/cpu/vulnerabilities/l1tf:Mitigation: PTE Inversion; VMX: conditional cache flushes, SMT disabled
/sys/devices/system/cpu/vulnerabilities/mds:Mitigation: Clear CPU buffers; SMT Host state unknown
/sys/devices/system/cpu/vulnerabilities/meltdown:Mitigation: PTI
/sys/devices/system/cpu/vulnerabilities/spec_store_bypass:Mitigation: Speculative Store Bypass disabled via prctl and seccomp
/sys/devices/system/cpu/vulnerabilities/spectre_v1:Mitigation: usercopy/swapgs barriers and __user pointer sanitization
/sys/devices/system/cpu/vulnerabilities/spectre_v2:Mitigation: Full generic retpoline, IBPB: conditional, IBRS_FW, STIBP: disabled, RSB filling
/sys/devices/system/cpu/vulnerabilities/srbds:Unknown: Dependent on hypervisor status
/sys/devices/system/cpu/vulnerabilities/tsx_async_abort:Not affected
```

The most famous are Meltdown / Spectre:

From [https://meltdownattack.com/](https://meltdownattack.com/):

> Meltdown and Spectre exploit critical vulnerabilities in modern processors. These hardware vulnerabilities allow programs to steal data which is currently processed on the computer. While programs are typically not permitted to read data from other programs, a malicious program can exploit Meltdown and Spectre to get hold of secrets stored in the memory of other running programs. This might include your passwords stored in a password manager or browser, your personal photos, emails, instant messages and even business-critical documents.
> 
> Meltdown and Spectre work on personal computers, mobile devices, and in the cloud. Depending on the cloud provider's infrastructure, it might be possible to steal data from other customers.

All Linux distributions are now protected against these flaw but we lost some power some programs are running a bit slower than before.

## How to disable these mitigations to make Linux fast again

There is a funny website for this: [https://make-linux-fast-again.com/](https://make-linux-fast-again.com/)

Open `/etc/default/grub` and edit the `GRUB_CMDLINE_LINUX` kernel parameter.

If you are using Linux Kernel version 5.1.13 and newer:

```
GRUB_CMDLINE_LINUX="mitigations=off"
```

If you are using older versin than 5.1.13:

```
GRUB_CMDLINE_LINUX="noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off mitigations=off"
```

Don't forget to update grub files, otherwise it won't be applied !

```
$ sudo update-grub
```

Reboot and enjoy, your system is maybe faster, but vulnerable:

```
$ lscpu
(...)
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Mitigation; PTE Inversion; VMX vulnerable, SMT disabled
Vulnerability Mds:               Vulnerable; SMT Host state unknown
Vulnerability Meltdown:          Vulnerable
Vulnerability Spec store bypass: Vulnerable
Vulnerability Spectre v1:        Vulnerable: __user pointer sanitization and usercopy barriers only; no swapgs barriers
Vulnerability Spectre v2:        Vulnerable, IBPB: disabled, STIBP: disabled
Vulnerability Srbds:             Unknown: Dependent on hypervisor status
Vulnerability Tsx async abort:   Not affected

$ grep . /sys/devices/system/cpu/vulnerabilities/*
/sys/devices/system/cpu/vulnerabilities/itlb_multihit:Not affected
/sys/devices/system/cpu/vulnerabilities/l1tf:Mitigation: PTE Inversion; VMX: vulnerable, SMT disabled
/sys/devices/system/cpu/vulnerabilities/mds:Vulnerable; SMT Host state unknown
/sys/devices/system/cpu/vulnerabilities/meltdown:Vulnerable
/sys/devices/system/cpu/vulnerabilities/spec_store_bypass:Vulnerable
/sys/devices/system/cpu/vulnerabilities/spectre_v1:Vulnerable: __user pointer sanitization and usercopy barriers only; no swapgs barriers
/sys/devices/system/cpu/vulnerabilities/spectre_v2:Vulnerable, IBPB: disabled, STIBP: disabled
/sys/devices/system/cpu/vulnerabilities/srbds:Unknown: Dependent on hypervisor status
/sys/devices/system/cpu/vulnerabilities/tsx_async_abort:Not affected
```
