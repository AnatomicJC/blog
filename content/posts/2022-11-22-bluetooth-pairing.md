---
title: "Bluetooth pairing with command line"
author: JC
date: 2022-11-22T00:00:01+00:00
url: /cli-bluetooth-pairing
description: How to manually pair bluetooth device with command line
cover:
  image: /images/bluetooth/bluetooth.jpg
tags:
  - memo
  - bluetooth

ShowToc: true
TocOpen: false
---

Source: https://askubuntu.com/questions/1225896/huawei-freebuds-3-pairing-with-ubuntu-18-04

Open the bluetooth controller and check its working:

```shell
bluetoothctl
show
```

This should provide you with a name, alias and some other parameters. If not, then the following won't work and you'll need to check the bluetooth service/device.

Run the following:

```shell
agent on
default-agent
power on
pairable on
```

These may already be configured like this, but it doesn't hurt to run them anyway.

Check if your device is already registered:

```shell
devices
```

If your device is listed:

```shell
remove [MAC_ADDRESS]
```

***Replace [MAC_ADDRESS] with your device's MAC Address

Put your device into pairing mode and then:

```shell
pair [MAC_ADDRESS]
connect [MAC_ADDRESS]
trust [MAC_ADDRESS]
```

If this works, then you can exit with:

```
exit
```
