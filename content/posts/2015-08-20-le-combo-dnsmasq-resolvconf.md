---
title: Le combo dnsmasq / resolvconf
description: dnsmasq resolvconf
author: JC
date: 2015-08-20T17:54:37+00:00
url: /le-combo-dnsmasq-resolvconf
tags:
  - Memos
  - DNS

---
# Le problème:

Il y a un serveur DNS au taff qui ne résoud que les noms de machines internes, pas le net  

Si je mets ce serveur en premier dans /etc/resolv.conf, et que je ne suis plus au taff, les requètes DNS prennent des plombent car elles vont tout d&rsquo;abord essayer de requêter ce serveur qui n&rsquo;est plus dans mon réseau.  

Si je ne mets pas ce serveur en premier dans /etc/resolv.conf, les machines locales du taff ne resolvent pas car elles n&rsquo;existent pas dans le cache DNS du FAI.  

Je ne gère pas le réseau de mon taff.

# La solution: le combo resolvconf + dnsmasq

dnsmasq est un cache DNS local et resolvconf gère le fichier /etc/resolv.conf (je la fais courte, hein !). En gros, toutes vos requètes DNS vont passer par votre cache local, et dnsmasq se démerde pour resoudre vos machines sur le bon serveur DNS, sans latence.

Donc:

```
# apt install dnsmasq resolvconf
```

Une fois ces paquets installés, votre `/etc/resolv.conf` ressemblera à ceci:

```
nameserver 127.0.0.1
```

Dans /etc/network/interfaces, si vous utilisez les DNS de Google et que votre DNS du taff est 192.168.23.7, ajoutez cette ligne,

```
dns-nameservers 8.8.8.8 8.8.4.4 192.168.23.7
```

Relancez:

```
# service networking restart
```

Et c&rsquo;est tout bon !

Si vous utilisez ce roublard de network-manager pour spécifier vos serveurs DNS, un peu de conf en plus:

```
# mkdir /etc/NetworkManager/dnsmasq.d/  
# echo cache-size=1000 > /etc/NetworkManager/dnsmasq.d/cache
```

Puis ajoutez `dns=dnsmasq` dans la section [main] de /etc/NetworkManager/NetworkManager.conf:

```
echo listen-address=::1 > /etc/NetworkManager/NetworkManager.conf
```

Vous retrouverez ces infos ici: https://wiki.archlinux.org/index.php/Dnsmasq#NetworkManager

```
[main]  
plugins=keyfile  
dns=dnsmasq
```

Si vous utilisez de l&rsquo;IPv6, pensez à ajouter le fichier `/etc/NetworkManager/dnsmasq.d/ipv6_listen.conf`

Dans le doute, relancez les services dnsmasq, network-manager et resolvconf !
