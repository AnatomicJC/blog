---
title: 'OpenERP: mot de passe oublié'
author: JC
date: 2010-11-03T08:24:51+00:00
url: /openerp-mot-de-passe-oublie/
tags:
  - openerp

---
Il peut arriver que l&rsquo;on ne se souvienne plus de son mot de passe administrateur OpenERP. Voici comment le retrouver.

Logguez-vous via ssh (ou autre) sur le serveur où se trouve votre base de données. Si vous travaillez en local, c&rsquo;est localhost :-p

Connectez-vous en tant qu&rsquo;utilisateur postgres:

```
$ sudo su - postgres
```

Connectez-vous à votre base de données:

```
$ psql le_nom_de_ma_base_de_données
```

Il ne vous reste plus qu&rsquo;à executer la requète suivante (n&rsquo;oubliez pas le point-virgule à la fin de la requète):

```
SELECT login, password FROM res_users;
```

A vous d&rsquo;adapter cette requête en fonction du login recherché (admin, demo, ou je ne sais quoi d&rsquo;autre).
