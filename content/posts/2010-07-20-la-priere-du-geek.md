---
title: La prière du Geek
author: JC
date: 2010-07-20T20:25:27+00:00
url: /la-priere-du-geek/
categories:
  - En vrac
tags:
  - Histoires

---
**Version Linuxienne :**

 > Notre torwald qui es sur le net  
 > Que ton nom soit affiché  
 > Que linux vienne  
 > Que ton noyau soit compilé avec ou sans options  
 > Donne nous aujourd’hui notre code de ce jour  
 > Pardonne nous nos bugs, comme nous pardonnons aussi aux Freeware trops buggés  
 > Et ne nous soumets pas a la crosoftisation  
 > Et délivre nous de windows  
 > Amen

**Version Windowsienne :**

 > Notre Gates, qui est à Seattle,  
 > Que Ton Windows soit débogué,  
 > Que Ton monopole s’impose,  
 > Que Tes commandes soient exécutées  
 > Sur le Web comme sur le disque dur.  
 > Donne-nous aujourd’hui  
 > Nos mises à jour quotidiennes  
 > Et pardonne-nous nos utilisations de Linux  
 > Comme nous pardonnons aussi  
 > A ceux qui ont utilisé des Macs.  
 > Et ne nous soumets pas au Dr Norton  
 > Mais délivre nous du plantage  
 > Car c’est à toi qu’appartiennent  
 > Le Copyright, les Mégahertz et les Capitaux.  
 > Amen

Source : [bashfr.org][1]

 [1]: http://bashfr.org/?5394 "Bashfr.org"
