---
title: Convertir des photos en une vidéo
description: Convertir des photos en une vidéo
author: JC
date: 2013-01-13T13:14:57+00:00
url: /convertir-des-photos-en-une-video/
featured_image: /wp-content/uploads/2013/01/video-pivoter.jpg
categories:
  - En vrac
tags:
  - Memos

---
Un petit mémo rapide pour me souvenir comment convertir toute une série de photos en vidéo. Ça se passe avec mencoder:

```bash
mencoder -ovc copy -mf w=640:h=480:fps=12:type=jpg 'mf://*.jpg' -o output.avi
```
