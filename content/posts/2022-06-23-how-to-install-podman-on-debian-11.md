---
title: "How to setup podman in rootless mode on Debian 11"
author: JC
date: 2022-06-23T00:00:01+00:00
url: /how-to-setup-podman-on-debian-11
description: How to install podman with podman-compose on Debian 11
cover:
  image: /images/podman/podman.svg
tags:
  - podman
  - podman-compose
  - docker
  - containers
  - debian
  - memo

ShowToc: true
TocOpen: true
---

## Podman ?

Podman is a daemonless container engine for developing, managing, and running Open Container Initiative (OCI) 
containers and container images on your Linux System. 

Podman provides a Docker-compatible command line front end that can simply alias the Docker cli, **alias docker=podman**.
You can know more on [their website](https://podman.io/whatis.html).

If you want to use podman / podman-compose in place of docker / docker-compose on Debian, you are reading the right how-to :-)

## Remove docker

If you already have docker installed, you can remove it with these commands:

```shell
sudo apt remove --purge $(dpkg -l | grep -E "(docker|containerd)" | awk '{print $2}')
sudo apt autoremove --purge
```

## Setup podman

### Configure libcontainers repository

We will configure first the libcontainers repository who contains the latest stable version of podman.

Retrieve the repository GPG signing key and put it on `/usr/share/keyrings/libcontainers.gpg`:

```shell
curl -s https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/Release.key | gpg --dearmor | sudo tee /usr/share/keyrings/libcontainers.gpg > /dev/null
```

Configure the repository in the new DEB822 format on `/etc/apt/sources.list.d/libcontainers.sources`:

```shell
cat << EOF | sudo tee /etc/apt/sources.list.d/libcontainers.sources > /dev/null
Types: deb
URIs: https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/
Suites: /
Signed-By: /usr/share/keyrings/libcontainers.gpg
EOF
```

### Install podman

Refresh your apt sources then install podman in rootless mode and all needed dependencies:

```shell
sudo apt install podman-rootless podman-plugins podman-machine-cni uidmap containernetworking-plugins python3-dotenv slirp4netns
```

You can now validate it is working by launching the hello-world container:

```shell
podman run -rm hello-world
```

### Configure subuid / subgid

According to [Gentoo's wiki](https://wiki.gentoo.org/wiki/Subuid_subgid):

> *SubUID/GIDs are a range subordinate user/group IDs that a user is allowed to use. These are commonly used by containerization software, such as LXD and Podman, for creating privilege separated containers.*

To be able to run rootless containers in your user scope, you need to configure `subuid` and `subgid`. Users created with `useradd` command on Debian have these entries by default.

If your user is not present in `/etc/subuid` and `/etc/subgid` files, use this command:

```shell
usermod --add-subuids 100000-165535 --add-subgids 100000-165535 your-username
```

Then run the `podman system migrate` command with your user to apply the change.

Ref: [https://wiki.archlinux.org/title/Podman#Configuration](https://wiki.archlinux.org/title/Podman#Configuration)

It is a very important setting. As an example, if you try to run `apt update` command in a rootless debian container without subuid / subgid properly set, you will get this error (Ref: [https://github.com/containers/podman/discussions/11217](https://github.com/containers/podman/discussions/11217)):
```shell
$  podman run --rm --runtime crun -it --entrypoint bash docker.io/library/ubuntu
root@be1d3f62fed7:/# apt-get update
E: setgroups 65534 failed - setgroups (1: Operation not permitted)
E: setegid 65534 failed - setegid (22: Invalid argument)
E: seteuid 100 failed - seteuid (22: Invalid argument)
E: setgroups 0 failed - setgroups (1: Operation not permitted)
Reading package lists... Done
W: chown to _apt:root of directory /var/lib/apt/lists/partial failed - SetupAPTPartialDirectory (22: Invalid argument)
W: chown to _apt:root of directory /var/lib/apt/lists/auxfiles failed - SetupAPTPartialDirectory (22: Invalid argument)
E: setgroups 65534 failed - setgroups (1: Operation not permitted)
E: setegid 65534 failed - setegid (22: Invalid argument)
E: seteuid 100 failed - seteuid (22: Invalid argument)
E: setgroups 0 failed - setgroups (1: Operation not permitted)
E: Method gave invalid 400 URI Failure message: Failed to setgroups - setgroups (1: Operation not permitted)
E: Method http has died unexpectedly!
E: Sub-process http returned an error code (112)
```

## Miscellaneous

### Additional configuration for systemd

You must set the cgroup_manager to cgroupfs if you are running systemd:

```shell
cat << EOF | sudo tee /etc/containers/containers.conf > /dev/null
[engine]
cgroup_manager = "cgroupfs"
events_logger = "file"
EOF
```

### Use privileged ports

If you launch a nginx container and try to expose the 80 port, you will have a warning that you can't use port < 1024.

To be able to use privileged ports as unprivileged user, you can set this sysctl rule:

```bash
# Set the rule
sudo cat << EOF | sudo tee /etc/sysctl.d/99-podman-rootless.conf
net.ipv4.ip_unprivileged_port_start=80
EOF
# Enable it
sudo sysctl --system
```

### Setup podman-compose

podman-compose is a python script you can retrieve like this:

```shell
sudo curl -so /usr/local/bin/podman-compose https://raw.githubusercontent.com/containers/podman-compose/devel/podman_compose.py
sudo chmod +x /usr/local/bin/podman-compose
```

That's it, you can now launch your docker-compose files with `podman-compose` \0/

### Where are stored my containers data ?

If you are a docker user, you are used to find your images and container data in one place: `/var/lib/docker`

With podman, the data are stored in each user's home, in `~/.local/share/containers`.
