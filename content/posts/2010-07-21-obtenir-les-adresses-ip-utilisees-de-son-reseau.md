---
title: Obtenir les adresses IP utilisées de son réseau
author: JC
date: 2010-07-21T14:32:52+00:00
url: /obtenir-les-adresses-ip-utilisees-de-son-reseau/
tags:
  - Memos
  - nmap

---
La commande suivante est exécutée sur mon réseau personnel, pensez à remplacer 192.168.0.0 par votre sous réseau :

```
$ nmap -T4 -sP 192.168.0.0/24
```

Affiche les adresses IP utilisées ainsi que leur adresse MAC.
