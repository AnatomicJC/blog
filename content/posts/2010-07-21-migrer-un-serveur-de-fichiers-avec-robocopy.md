---
title: Migrer un serveur de fichiers avec robocopy
author: JC
date: 2010-07-21T15:39:05+00:00
url: /migrer-un-serveur-de-fichiers-avec-robocopy/
tags:
  - Windows
  - Memos

---
source : http://syskb.com/migrer-un-serveur-de-fichiers-avec-robocopy/

Je vous propose une méthode de migration de fichiers basée sur l’utilisation de Robocopy. Il existe de nombreux outils, plus ou moins complexes, avec une interface plus ou moins attrayante, mais le plus performant de part ses options et ses caractéristiques reste selon moi Robocopy. En plus il est GRATUIT !

# Les préparatifs

Tout d’abord téléchargez et installez la dernière version de Robocopy disponible sur le Kit de Ressources Windows 2003 : http://www.microsoft.com/downloads/details.aspx?FamilyID=9D467A69-57FF-4AE7-96EE-B18C4790CFFD&displaylang=en

robocopy.exe sera installé dans C:\Program Files\Windows Resource Kits\Tools.
  
Si vous avez installé le Kit de ressources sur votre station d’administration, copiez robocopy.exe sur le C:\ du serveur sur lequel vous souhaitez migrer les données, votre nouveau serveur de fichiers.

Connectez vous sur le nouveau serveur de fichier et ouvrez une invite de commande.
  
Tapez cd\ afin de vous positionner sur le C:\ de votre nouveau serveur. Vous pouvez ainsi accéder à robocopy.exe
  
A noter que si vous installez le Kit de ressource directement sur votre nouveau serveur, celui ci sera référencé dans le “Path” du serveur ce qui rendra Robocopy accessible depuis n’importe quelle emplacement de votre ligne de commande.

# Le principe de la migration

Pourquoi prévoir une longue interruption de la production pour migrer plusieurs centaines de méga octets voir de téra octets ? il est possible de lancer la copie des fichiers du serveur source vers le serveur cible plusieurs jours en avance puis de réaliser la mises à jour des modifications au fil de l’eau.
  
Ainsi le jour J il ne reste plus qu’à couper l’accès aux utilisateurs à l’ancien serveur, lancer une dernière synchronisation, puis d’établir l’accès sur le nouveau.
  
Vous pouvez même vous simplifier la vie en redonnant l’ancien nom de serveur au nouveau serveur, ce qui vous évitera de mettre en place des scripts de migration des lecteurs réseaux de vos utilisateurs.

# Construire son script

Robocopy propose de nombreuses options parfois complexes et qui seront bien souvent inutiles. Concrètement lorsque l’on veut déplacer des données d’un serveur vers un autre, l’idéal est de conserver exactement la même arborescence et de récupérer les droits NTFS associés.
  
Pour lancer la copie d’un partage source vers un partage cible il faut lancer la commande suivante:
  
```
robocopy "\\serveursource\share" "\\serveurcible\share"
```
  
Mais il sera possible d’agrémenter cette commande grâce à de nombreuses fonctionnalités !
  
/MIR : Cette option permet de reporter les changements de la source vers la cible. Si un fichier est créé sur la source il va être copié vers la cible. Si un fichier est supprimé sur la source, il le sera également sur la cible.
  
/SEC : Cette autre option géniale permet de migrer également les droits NTFS !
  
/LOG : Autant garder un historique de la copie. Cette option désactive le “verbose” à l’écran. Voir l’option qui suit !
  
/TEE : Lorsque l’on utilise /LOG l’intéractivité à l’écran peut être conservé grâce à cette option
  
/MON:x : Permet de lancer la commande en mode monitor, c’est à dire que la commande va rester active et attendre un certain nombre x de changements apportés sur la source pour les copier sur la cible. Ainsi la première fois tout va être copié et la commande va attendre les changements.
  
/RH:hhmm-hhmm : Cette option permet de lancer le script qu’à une certaine plage horaire, très pratique si vous préférez lancer la copie la nuit.
  
/MOT:x : Idem que /MON mais cette fois x correspond à un nombre de minutes d’attente avant de relancer la synchro.
  
Grâce à ces quelques options, il en existe beaucoup d’autres, j’effectue mes migrations très simplement.
  
Je lance mes commandes depuis une invite de commande ou depuis un fichier batch si j’ai plusieurs partages localisés un peu n’importe ou …
  
Quelques jours avant le changement de serveur je lance ma première copie grâce à la commande suivante :
  
```
robocopy "\\serveursource\share" "\\serveurcible\share" /MIR /SEC /RH:2000-0700 /TEE /LOG+:c:\journal.log
```
  
De cette manière ma copie se déroule de 20h à 6h du matin. Si mon volume de données à copier est tel que cela n’a pas suffit ! Je peux le relancer une seconde fois …
  
Lorsque tout est copié, j’utilise plutôt la commande suivante:
  
```
robocopy "\\serveursource\share" "\\serveurcible\share" /MIR /SEC /MOT:10 /TEE /LOG+:c:\journal.log
```
  
Cette fois toutes les 10 mn les changements vont être synchronisés !
  
Et pour finir le jour de la bascule, je coupe l’accès aux utilisateurs et je lance :
  
```
robocopy "\\serveursource\share" "\\serveurcible\share" /MIR /SEC /TEE /LOG+:c:\journal.log
```
  
Ensuite je change le nom des serveurs afin de nommer mon nouveau serveur comme l’ancien et ma migration est terminée ! je prend soin de supprimer mon script afin de ne pas le lancer par accident …
