---
title: Reboot automatique après un kernel panic
description: Reboot automatique après un kernel panic
author: JC
date: 2010-07-21T14:24:50+00:00
url: /reboot-automatique-apres-un-kernel-panic/

tags:
  - kernel-panic
  - Memos

---
Source : http://www.marmottux.org/index.php/?q=reboot+automatique
  
Vous avez du matériel de provenance russe ?
  
Et vous avez souvent des Kernel Panic ?
  
Si vous n&rsquo;avez pas d&rsquo;accès physique au serveur (Exemple : vous êtes à 1000 Km), une solution simple s&rsquo;offre à vous :
  
Un reboot automatique de la machine après un kernel panic :
  
Editez le fichier :
  
```
vim /etc/sysctl.conf
```
  
Ajoutez la ligne :
  
```
kernel.panic = 10
```
  
Puis faites la prendre en compte par le kernel :
  
```
sysctl -p
```
