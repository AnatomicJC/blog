---
title: CHMOD récursif
author: JC
date: 2010-11-26T10:03:47+00:00
url: /chmod-recursif/
tags:
  - Memos

---
Le mini-script ci-dessous va appliquer dans le répertoire courant:

  * les droits 755 sur tous les dossiers
  * les droits 644 sur tous les fichiers

A adapter selon les besoins:

```
#!/bin/bash
find -type d -exec chmod 755 {} \;
find -type f -exec chmod 644 {} \;
```
