---
title: Manage Ruby environments with RVM
author: JC
date: 2022-02-11T00:00:00+00:00
url: /manage-ruby-environments-with-rvm/
description: Manage Ruby environments with RVM
tags:
  - ruby

ShowToc: true
TocOpen: true
---

Let's say you have multiple ruby project on your laptop:

  * project A with Ruby 2.6
  * project B with Ruby 3.1 and dependencies in 5.x version
  * project C with Ruby 3.1 too but dependencies in 8.x version

What a mess ! RVM to the rescue !

RVM is a tool who will let you manage your ruby environments

### Install RVM

```
$ curl -sSL https://get.rvm.io | bash
```

Open a new terminal to be able to use it.


### Get available ruby versions

```
$ rvm list known
```

### Install multiple versions

Each ruby version you will install will be located on your home folder.

For the 3 version:

```
rvm install 3.1
```

For the 2.6 version

```
rvm install 2.6
```

### Use a given version

```
rvm use 2.6
```

### Create isolated environments

#### Project A

Let's say you want to work on your project A with ruby 2.6.

Switch to ruby 2.6

```
rvm use 2.6
```

Create an isolated 2.6 environment for your project. It is called a gemset:

```
rvm gemset create project-a
```

Use it:

```
rvm gemset use project-a
```

Then install your gems:

```
bundle install
```

#### Project B

Let's switch to your B project who is using Ruby 3.1

```
rvm use 3.1
```

And create an isolated 3 environment for your project.

```
rvm gemset create project-b
```

Use it:

```
rvm gemset use project-b
```

Then install your gems:

```
bundle install
```

### RVM configuration files

You can make rvm automatically switch to your ruby environment and version automagically depending the project you are located in by creating a rvmrc file.

Create a `.rvmrc` file:

```
rvm rvmrc create
```

Trust your file:

```
rvm rvmrc trust
```

Convert configuration file to ruby version:

```
rvm rvmrc to ruby-version
```

### Conclusion

You can now easily switch between your ruby environments.

First switch to a ruby version, then you can list available gemsets for a given ruby version with this command:

```
rvm gemset list
```

If you want to go back to ruby provided by your operating system:

```
rvm use system
```
