---
title: 'Danse de salon et bal rétro: dans quel ordre enchaîner les séries de morceaux ?'
description: 'Danse de salon et bal rétro: dans quel ordre enchaîner les séries de morceaux ?'
author: JC
date: 2016-08-04T16:35:58+00:00
url: /danse-de-salon-et-bal-retro-dans-quel-ordre-enchainer-les-series-de-morceaux/
featured_image: /wp-content/uploads/2016/08/salsafeet-825x510.jpg
categories:
  - En vrac

---
Ce n&rsquo;est pas un secret ni une recette magique et ça dépend un peu de la manière de faire de chacun.

Traditionnellement, on ouvre la bal avec un paso-doble. Viennent ensuite tango, valse, cha-cha, rock, bolero. On commence toujours à peu près de cette manière.  
Après on recommence paso tango valse cha-cha, rock, bolero&#8230;

MAIS :

On ajuste ensuite en fonction du public et toujours en fonction du public et selon l&rsquo;ambiance de l&rsquo;après-midi ou soirée.  
On chamboule un peu cet ordre de temps en temps. Et il faut que toutes ces danses aient été faites au moins une fois dans une heure.

Le tout étant de ne pas faire trop longtemps des danses rapides ou des danses lentes, il faut alterner. Si tu viens d&rsquo;enchaîner une série de rocks endiablés et que tu vois que les gens n&rsquo;en peuvent plus, il faut enchaîner (par exemple) par un tango qui sera plus reposant.

Il faut ensuite intercaler dans cet ordre pré-établi d&rsquo;autres danses. Il faut au moins faire 1 ou 2 madisons dans la soirée qui peut se mettre à la place ou avant d&rsquo;une série de rock.

A la place du rock ou du cha-cha, tu peux glisser une série de fox-trot ou charleston.

A la place des boleros (chansons anciennes), tu peux faire une série de slows (chansons + récentes).

Une à deux fois par heure (tout dépend du public), il faut au moins faire une série d&rsquo;ambiance d&rsquo;environ 10 minutes (ça peut être des tubes de maintenant ou des tubes de l&rsquo;été récents ou plus anciens). Il faut comprendre par série d&rsquo;ambiance morceaux qui bougent, ça peut aller des tubes du moment à la compagnie créole en passant par des salsas&#8230;

En général en retro, il y a également le couple java + polka.

La java étant à 3 temps, ne pas enchaîner une valse avec une java. Éviter également de l&rsquo;enchaîner avec un boston qui est également une valse lente à 3 temps.

Des fois on ne sait pas quoi enchaîner comme série. Heureusement, on a des gens qui viennent nous voir : « vous pourrez nous faire une valse viennoise ? » et on répond « bien sûr !! ».

Après, le public peut se diviser en plusieurs catégories : il y a ceux qui viennent en couple ou avec des amis passer un bon moment. Il faut varier les danses. Il y a les vrais danseurs qui viennent pour danser. Ça, c&rsquo;est un régal pour les yeux, il faut toujours varier.  
Après, il peut y avoir une majorité de personnes seules qui viennent pour « draguer », ou des hommes mariés qui viennent sans leur femme pour trouver une maîtresse, tu vois le genre. Là, il faut privilégier les séries d&rsquo;ambiance et celles un peu sportives (rock), où ces messieurs pourront faire un démonstration de leurs prouesses. Une fois qu&rsquo;ils auront impressionné leur belle, il faut qu&rsquo;ils puissent conclure sur un slow, boléro ou tango s&rsquo;il savent le danser.

Dans certaines soirées, il peut y avoir une majorité de femmes seules, là il ne faudra pas les oublier en faisant des morceaux d&rsquo;ambiance où on est pas obligé de danser à 2 car elles viennent aussi pour s&rsquo;éclater.

Il y a également des danses typiques à ne pas oublier suivant la région ou tu te trouves.  
En auvergne, ils aiment bien la bourée, je crois. Ce n&rsquo;est pas une danse que tu feras à Marseille.  
A Marseille, justement, ils aiment la java « à petits pas ». Ils font des petits pas donc si tes javas sont trop rapides, ils vont râler. Il faut vraiment ralentir le tempo.  
A Avignon, ils vont râler si tu fais tes javas trop lentes.  
Du côté d&rsquo;Alès, il ne faut pas oublier de faire la polka du brise-pied.  
A Montélimar, ça nous arrive de faire 3 ou 4 séries de madisons, ils adorent ça.  
A Mouriès et Eygalières, tu ne re-signes pas si tu ne joues pas la chanson du ricard + la vache, mais là, ce n&rsquo;est plus du retro&#8230;

Pour toutes les danses retro (pasos, tangos, etc.), on fait en général des série de 2 : on fait toujours 2 pasos, 2 tangos, 2 valses, 2 cha-chas pour une durée d&rsquo;environ 5 minutes.

Mais on fait en général 3 rocks avec un point super important, on va progressivement du plus lent (pour commencer cool) au plus rapide (pour les sportifs !).  
On fait également 3 slows (voire 4 parfois, plus rare).  
Pour les séries d&rsquo;ambiance, ça va par 3 également (voire 4).

C&rsquo;est une recette de Mr Raymond Echeverria.  
42 ans de métier, rien que ça.
