---
title: Paste a predefined text via a keyboard shortcut
description: Paste a predefined text via a keyboard shortcut
author: JC
date: 2021-03-25T21:24:02+00:00
url: /paste-predefined-text-via-keyboard-shortcut/
tags:
  - Memos
  - Kubernetes

---

> The aim of this tip is to have a keyboard shortcut to quickly configure my favorite kubectl aliases for the CKA exam :-)

Install `xdotool` and `xsel`

```
sudo apt install xdotool xsel
```

Add this keyboard shortcut (In Gnome: Settings > Keyboard > Customize Shortcuts > Custom Shortcut)

```
/bin/bash -c 'echo -n "source <(kubectl completion bash) && alias k=kubectl && complete -F __start_kubectl k && alias kgp=\"kubectl get pods\"" | xsel && xdotool click 2'
```

Source: https://askubuntu.com/questions/754903/using-a-keyboard-shortcut-to-paste-text
