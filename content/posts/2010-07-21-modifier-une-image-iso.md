---
title: Modifier une image iso
author: JC
date: 2010-07-21T12:13:13+00:00
url: /modifier-une-image-iso/
tags:
  - Memos
  - Image ISO

---
Ce memo explique comment modifier le contenu d&rsquo;une image iso existante.

Souce: http://linux.jpvweb.com/mesrecetteslinux/images_iso_cd


## Répertoires de travail

Créer 2 répertoires <strong>cdiso</strong> et <strong>cdiso2</strong>
  
Monter l&rsquo;image disque à modifier :
  
```
# mount -o loop -t iso9660 isoAModifier.iso cdiso/
```
  
Copier le contenu du répertoire cdiso vers cdiso2
  
```
# rsync -a cdiso/ cdiso2/
```
  
Puis se rendre dans cdiso2.

## Modifications puis construction

Faire toutes les modifications nécessaires puis reconstruire l&rsquo;iso. Lancer la commande suivante depuis le répertoire cdiso2 (ne pas oublier le point en fin de ligne !)
  
```
# mkisofs -o nouvelleImage.iso -J -r -v -V nomDeLImage -P "Description de l image" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-info-table -boot-load-size 4 .
```

### Options

 * -J pour l&rsquo;extention Joliet (compatibilité windows)
 * -r pour l&rsquo;extention “Rock Ridge” (compatibilité unix-linux)
 * -v mode verbeux
 * -o suivi du nom de l'image (le /chemin/ doit exister avant!)
 * -V suivi du nom de volume du cd
  
Options supplémentaires possibles, en particulier pour un cd amorçable avec isolinux:
  
 * -P suivi du nom de l&rsquo;éditeur (ex: -P “Suse Linux AG”)
 * -b suivi du /chemin/isolinux.bin pour l&rsquo;amorçage
 * -c suivi du /chemin/boot.cat pour créer un catalogue de boot
 * -no-emul-boot : pas d&rsquo;émulation de boot
 * -boot-load-size 4 : fixe la taille correcte du secteur de boot (compatibilité avec des anciens pc).
 * -boot-info-table : crée une table pour la piste d&rsquo;amorçage.
