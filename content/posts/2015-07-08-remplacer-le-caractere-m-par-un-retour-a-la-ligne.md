---
title: Lorsque dos2unix ne fonctionne pas...
description: dos2unix workaround
author: JC
date: 2021-01-24T08:34:15+00:00
url: /lorsque-dos2unix-ne-fonctionne-pas/
tags:
  - Memos

---
Je me sers de `dos2unix` pour remplacer le caractère ^M que l'on retrouve partout dans un fichier texte édité sous Windows.

Il peut arriver parfois que ça ne fonctionne pas, j'utilise alors ce workaround:

```
# remove ^M character, dos2unix is propably not installed so workaround
tr -d $'\r' < ${FILE} > seq_tmp
mv seq_tmp ${FILE}
rm seq_tmp
```

Si ça ne fonctionne toujours pas, sed à la rescousse:

```
sed -i 's/'"$(printf '\015')"'/\n/g' vilain_fichier.txt
```
