---
title: Configuration d'un serveur mail avec postfix
description: Configuration d'un serveur mail avec postfix
author: JC
date: 2010-07-21T10:24:17+00:00
url: /configuration-dun-serveur-mail-avec-postfix/
ShowToc: true
TocOpen: true
categories:
  - En vrac
tags:
  - postfix
  - Tutos

---
Vous ne voulez plus confier vos mails à Gmail, Yahoo ou Hotmail ?

Vous aimeriez héberger vos mails chez vous mais vous pensez que c&rsquo;est trop dur, trop compliqué ?
    
Vous pensez qu&rsquo;il faut être ingénieur pour monter un serveur mail ?
    
Que nenni !! Nous allons voir dans ce tuto la mise en place d&rsquo;un serveur mail avec Postfix dans le cadre d&rsquo;une utilisation personnelle.<!--more-->
    
Il ne s&rsquo;agit pas ici de concurrencer Gmail ou Yahoo mais bien d&rsquo;avoir notre petite boîte mail bien à nous. Faite avec amour. Et qui marche !!
    
Ici, pas de gestion par base <acronym title="Structured Query Language">SQL</acronym> ou annuaire <acronym title="Lightweight Directory Access Protocol">LDAP</acronym>, je vais tâcher de faire le plus simple possible.

Je me suis basé sur les paquets fournis par Debian 7.0 Wheezy.

C&rsquo;est parti !!

# Une adresse mail, c&rsquo;est quoi ?

## kevin@wanadoo.fr

Une adresse mail, c&rsquo;est un peu comme une adresse postale. Elle se divise en 2 parties :

 * un domaine : wanadoo.fr
 * un utilisateur : kevin

Dans mon exemple, lorsque l&rsquo;on écrit à Kevin via l&rsquo;adresse **kevin@wanadoo.fr**, cela signifie que l&rsquo;on écrit à l&rsquo;utilisateur **kevin** du domaine **wanadoo.fr**.

Plus précisément, le mail arrive sur la **machine** sur laquelle se trouve le **serveur mail** de wanadoo.fr, pour être ensuite remis dans la **boîte mail** de l&rsquo;utilisateur kevin.

## Identifier le serveur mail de wanadoo.fr

Wanadoo.fr, c&rsquo;est le domaine. Le service mail se trouve quant à lui sur une machine bien precise. Pour l&rsquo;identifier, une commande bien pratique :
  
```
$ host -t MX wanadoo.fr
wanadoo.fr mail is handled by 10 smtp.wanadoo.fr.
```

**smtp** est un *sous-domaine* de **wanadoo.fr**, et la machine sur laquelle se trouve le serveur mail se nomme justement **smtp.wanadoo.fr**…
  
```
$ ping smtp.wanadoo.fr
PING smtp.wanadoo.fr (80.12.242.142) 56(84) bytes of data.
```
  
… et son adresse IP est **80.12.242.142**.
  
Dans la réalité, l&rsquo;architecture mail de Wanadoo (Orange maintenant…) est je pense un peu plus compliquée que ça mais ça donne une bonne image de ce que nous allons reproduire pour notre serveur mail.

# Prérequis

Pour avoir son serveur mail, il faut donc :

 * un nom de domaine
 * une IP fixe
 * un PC allumé si possible 24h/24

Pourquoi une IP fixe ? Les mails doivent pouvoir toujours trouver le serveur mail, c&rsquo;est pourquoi l&rsquo;IP doit être fixe.

L&rsquo;association nom de domaine ⇔ adresse IP se règle au niveau du serveur <acronym title="Domain Name System">DNS</acronym>. C&rsquo;est celui-ci que nous allons régler en premier.

## Configurer le serveur DNS

Cette partie se révèle souvent être une prise de tête pour les apprentis auto-hébergés.

On doit spécifier dans le serveur <acronym title="Domain Name System">DNS</acronym> sur quelle IP on peut joindre le serveur mail.

La plupart des registrars (les marchands de noms de domaine) fournissent une interface simplifiée pour le serveur <acronym title="Domain Name System">DNS</acronym>.

### Le champ A

Il faut tout d&rsquo;abord configurer le champ **A** qui fait le lien entre nom de domaine et l&rsquo;adresse IP du serveur :
  
```
@       IN    A    12.34.56.78
smtp    IN    A    12.34.56.78
```
  
**@** : signifie que **monDomaine.com** est lié à l&rsquo;IP 12.34.56.78

**smtp** : signifie que le sous-domaine **smtp.monDomaine.com** est lui aussi lié à l&rsquo;IP 12.34.56.78

Vous pouvez indiquer des adresses IP différentes si vous possédez plusieurs serveurs et plusieurs IP.

### Le champ MX

Une fois que le champ **A** est complété, il faut pour définir le serveur mail s&rsquo;occuper du champ **MX** (**M**ail e**X**change).

Nous allons convenir que le serveur mail correspond au serveur **smtp.monDomaine.com**. Son adresse IP ayant déjà été définie par le champ **A**, le champ **MX** s&rsquo;écrit sous cette forme :
  
```
monDomaine.com.    IN    MX    10    smtp
```
  
Ce qui signifie que **smtp** du champ **A** sera le serveur **MX** de **monDomaine.com**.
  
Vous suivez toujours ??
  
Lorsque vous enverrez des mails sur le domaine **monDomaine.com**, ils seront automatiquement acheminés vers le serveur **smtp.monDomaine.com**.
  
***Important :***
  
Un champ **MX** se définit toujours sur un champ **A**, pas sur un **CNAME** (non abordé ici)

Ne pas oublier le point après monDomaine.com.

## Vérifications

Avant même d&rsquo;installer et configurer son serveur, il faut faire quelques vérifications.
  
```sh
$ host -t MX monDomaine.com
```
  
Cette commande doit retourner une ligne de ce genre :
  
```sh
monDomaine.com mail is handled by 10 smtp.monDomaine.com
```
  
Bien sûr, *smtp.monDomaine.com* doit être associé à votre IP fixe (ici **12.34.56.78**) :
  
```
$ ping smtp.monDomaine.com
PING smtp.monDomaine.com (12.34.56.78) 56(84) bytes of data.
```
  
Si ces 2 vérifications ne fonctionnent pas, **ce n&rsquo;est pas la peine d&rsquo;aller plus loin**, votre serveur <acronym title="Domain Name System">DNS</acronym> est mal configuré.

# Ouverture des ports

Si votre serveur se trouve derrière une *box, il ne pourra pas communiquer avec le monde extérieur. Il nous faut **ouvrir des ports**.

Votre *box possède une **IP fixe publique**, c&rsquo;est l&rsquo;IP que vous avez normalement renseignée dans votre serveur <acronym title="Domain Name System">DNS</acronym>.

Derrière votre *box, il y a votre serveur mail qui lui possède également une adresse IP, **privée**. Les adresses privées sont en général 192.168.0.xxx ou 192.168.1.xxx.

Admettons que votre serveur aie comme IP 192.168.0.25 dans votre réseau privé. Il faut alors que votre *box redirige toutes les requètes mail vers l&rsquo;IP privée 192.168.0.25.

Les ports à ouvrir sont :
  
 * 25 : pour que le serveur réceptionne les mails
 * 110 : pour relever ses mails via <acronym title="Post Office Protocol">POP</acronym>
 * 143 : pour relever ses mails via <acronym title="Internet Message Access Protocol">IMAP</acronym>
  
Tous ces ports sont à ouvrir en **TCP**.
  
Freebox : <a title="http://www.dslvalley.com/dossiers/freebox/freebox-nat.php" href="http://www.dslvalley.com/dossiers/freebox/freebox-nat.php" rel="nofollow">http://www.dslvalley.com/dossiers/freebox/freebox-nat.php</a>
  
NeufBox : <a title="http://www.dslvalley.com/dossiers/9box4/neufbox-nat-emule.php" href="http://www.dslvalley.com/dossiers/9box4/neufbox-nat-emule.php" rel="nofollow">http://www.dslvalley.com/dossiers/9box4/neufbox-nat-emule.php</a> (fin de page)
  
LiveBox : <a title="http://www.dslvalley.com/dossiers/orange/livebox-emule.php" href="http://www.dslvalley.com/dossiers/orange/livebox-emule.php" rel="nofollow">http://www.dslvalley.com/dossiers/orange/livebox-emule.php</a> (fin de page)

# Installation de postfix

Votre <acronym title="Domain Name System">DNS</acronym> est correctement configuré ? Les ports sont ouverts ? Bien. Vous avez fait le plus dur.
  
Vérifiez le nom de votre machine :
  
```
$ hostname
```
  
Cette commande doit vous retourner **smtp.monDomaine.com**. Si ce n&rsquo;est pas le cas, éditez le fichier **/etc/hosts** et vérifiez que vous avez une ligne comme suit :
  
```
127.0.0.1 smtp.monDomaine.com smtp
```
  
Puis :
  
```
# hostname smtp.monDomaine.com
```
  
Normalement, le hostname est maintenant OK.

Il est important que le **hostname** de votre machine soit le même que celui retourné par la commande **host -t MX monDomaine.com**, sinon Postfix va vous emmerder.

Installons postfix :
  
```
# apt install postfix
```
  
Choisissez la configuration **Internet Site**. Indiquez ensuite le nom de votre machine : **smtp.monDomaine.com** (correspond au *hostname*)
  
Vérifiez par la suite que la directive mydestination soit complète. Cette ligne doit contenir 7 valeurs.
  
 * Votre nom de domaine seul
 * Votre nom de machine (smtp) seul
 * votre nom de machine préfixé de votre domaine
 * votre nom de machine préfixé de localdomain
 * localhost
 * localhost préfixé de votre domaine
 * localhost préfixé de localdomain
  
example:
  
```
mydestination = monDomaine.com, smtp.monDomaine.com, smtp.localdomain, smtp, localhost.monDomaine.com, localhost.localdomain, localhost
```
  
Au cas où vous voudriez recommencer :
  
```
# dpkg-reconfigure postfix
```

# Configuration

Le fichier de configuration de postfix est **/etc/postfix/main.cf**. Il est par défaut très bien sécurisé.
  
Nous allons dans un premier temps y ajouter cette ligne :
  
```
home_mailbox = Maildir/
```
  
Cette ligne dit à postfix que les mails des utilisateurs doivent aller dans un répertoire nommé Maildir.
  
Commentez (ajoutez un #) la ligne où se trouve **mailbox_command** pour que procmail ne soit pas utilisé (en tout cas pas pour le moment):
  
```
#mailbox_command = procmail -a "$EXTENSION"
```
  
Pour que la modification soit prise en compte, redémarrez postfix :
  
```
# /etc/init.d/postfix restart
```

# Création d&rsquo;un nouveau mail

Bien. Vous avez votre domaine, votre serveur mail tourne. Créer une adresse mail est très simple. Il suffit d&rsquo;ajouter un utilisateur !!
  
Vous voulez créer une adresse kevin@monDomaine.com ? Ajoutez l&rsquo;utilisateur kevin sur votre machine :
  
```
# adduser kevin
```
  
Répondez aux questions, choisissez un mot de passe à kevin et c&rsquo;est tout.

# Tester l&rsquo;adresse en local

Nous allons envoyer un mail à Kevin, vérifions que le paquet **mailx** est installé :
  
```
# aptitude install mailx
```
  
On envoie le mail :
  
```
# echo "Le contenu du mail" | mail -s "ceci est le sujet" kevin@monDomaine.com
```
  
Le mail sera automatiquement déposé dans le répertoire **Maildir** situé dans le home de kevin. S&rsquo;il n&rsquo;existe pas, le dossier **Maildir** sera automatiquement créé.
  
```
# ls /home/kevin
Maildir
```
  
N&rsquo;hésitez pas à consulter les logs pour voir tout ce qui se passe sur votre serveur mail :
  
```
# cat /var/log/mail.log
```

# Kevin veut lire ses mails !!

Envoyer des mails, c&rsquo;est bien mais pouvoir les lire, c&rsquo;est mieux !! Pour que Kevin puisse lire ses mails dans un client comme **Mozilla Thunderbird**, c&rsquo;est tout simple :
  
Kevin veut réceptionner ces mails via <acronym title="Internet Message Access Protocol">IMAP</acronym> :
  
```
# aptitude install dovecot-imapd
```
  
Il préfère <acronym title="Post Office Protocol 3">POP3</acronym> ? Pas de problèmes :
  
```
# aptitude install dovecot-pop3d
```
  
Il faut également dire à dovecot où se trouve les mails des utilisateurs. Ouvrez le fichier /etc/dovecot/conf.d/10-mail.conf et remplacez:
  
```
mail_location = mbox:~/mail:INBOX=/var/mail/%u
```
  
par:
  
```
mail_location = maildir:~/Maildir
```
  
C&rsquo;est tout, vous pouvez maintenant utiliser votre client mail favori pour réceptionner vos mails.
  
 * Serveur : **smtp.monDomaine.com**
 * Login : **kevin**
 * Password : **le password défini lors de la création de l&rsquo;utilisateur kevin**

# Kevin veut envoyer des mails !!

Kevin est content, il peut lire ses mails. Seulement, il aimerait répondre à ses amis mais il ne peut pas.

## Utiliser le SMTP de son FAI

Postfix possède son propre service <acronym title="Simple Mail Transfer Protocol">SMTP</acronym>, mais ce dernier sera généralement bloqué. Pour limiter les envois de SPAMS, les FAI bloquent généralement l&rsquo;envoi de mails.
  
Pour que votre serveur mail puisse envoyer des mails, il faut lui dire d&rsquo;utiliser le <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> de votre FAI. C&rsquo;est le paramètre **relayhost** du fichier de configuration **/etc/postfix/main.cf** qu&rsquo;il faut modifier :
  
```
relayhost = smtp.votreFAI.com
```
  
Principaux <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> français :
  
 * Orange : smtp.orange.fr
 * Free : smtp.free.fr
 * Neuf : smtp.neuf.fr

## Utiliser le SMTP de Postfix

Cependant, certains FAI (comme Free) permettent de lever ce blocage via une option dans leur panel de gestion.
  
Dans ce cas, plus besoin du **relayhost**. Vos mails ne passeront plus par votre FAI, c&rsquo;est VOTRE serveur qui enverra directement les mails.
  
Le problème est que les mails que vous enverrez vers les boîtes **Yahoo** seront classées en tant que SPAM. **<acronym title="America Online">AOL</acronym>** ne voudra pas de vos mails (ce sera explicitement marqué dans les logs). Quant à **Hotmail**, les logs vous diront que les mails ont bien été reçus, mais vos correspondants ne les recevront jamais. **Gmail** ne pose quant à lui aucun soucis.
  
Vous voulez utiliser le plus possible votre propre <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> mais vous voulez quand même pouvoir envoyer des mails vers vos potes qui restés chez Hotmail ?
  
Créez un fichier **/etc/postfix/transport** avec ce contenu :
  
```
hotmail.fr    smtp:smtp.free.fr
hotmail.com   smtp:smtp.free.fr
yahoo.fr      smtp:smtp.free.fr
yahoo.com     smtp:smtp.free.fr
```
  
Ce fichier dit à postfix que pour les domaines hotmail.fr, hotmail.com, yahoo, etc., il utilisera le <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> de free. Pour tous les autres domaine, il utilisera le smtp local. A vous d&rsquo;adapter ce fichier en fonction des domaines qui ne veulent pas de vous.
  
Bien sûr, si vous n&rsquo;êtes pas abonné Free, vous ne pourrez pas utiliser le <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> de Free.
  
Pour que Postfix prenne en compte ce fichier, executez cette commande :
  
```
# postmap /etc/postfix/transport
```
  
Puis relancez Postfix :
  
```
# /etc/init.d/postfix restart
```
  
A chaque fois que vous modifierez **/etc/postfix/transport**, n&rsquo;oubliez pas d&rsquo;executer la commande précédente (postmap).

## Login&#8230; Passwd&#8230;

Actuellement, postfix n&rsquo;accepte d&rsquo;envoyer les mails que s&rsquo;ils sont envoyés depuis le serveur mail. Vous avez sûrement l&rsquo;habitude d&rsquo;envoyer les mails depuis un autre PC. Généralement, le <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> demande un login, un password et envoie vos mails.
  
C&rsquo;est ce que nous alons régler maintenant.
  
Problème : Postfix seul ne fournit pas de service d&rsquo;identification. Il doit utiliser un serveur SASL externe.
  
Solution : Dovecot, que nous avons précédemment installé pour la réception de mails (<acronym title="Internet Message Access Protocol">IMAP</acronym>&#8211;<acronym title="Post Office Protocol 3">POP3</acronym>) peut faire office de serveur SASL.
  
Pour activer ce service, dans le fichier de configuration **/etc/dovecot/conf.d/10-master.conf**, et décommentez la partie **Postfix smtp-auth** dans la section service auth { } :
  
```
service auth {
...
   # Postfix smtp-auth
    unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    }
...
}
```
  
Il s&rsquo;agit pour Dovecot de créer un socket Unix, sur lequel il écoutera les demandes d&rsquo;identification provenant de Postfix. Comme le service <acronym title="Simple Mail Transfer Protocol">SMTP</acronym> de Postfix est chrooté (emprisonné) dans le répertoire **/var/spool/postfix/**, il faut placer ce socket sous ce répertoire. Maintenant, relancez Dovecot :
  
```
# /etc/init.d/dovecot restart
```
  
Dovecot accepte l&rsquo;identification mais Postfix ne le sait pas !! Il faut lui dire (ajoutez dans **/etc/postfix/main.cf**) :
  
```
# Activer l'identification SASL
smtpd_sasl_auth_enable = yes

# Utiliser le service d'identification de Dovecot
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth

# Noter dans les en-tête des messages l'identifiant de l'utilisateur.
smtpd_sasl_authenticated_header = yes
```
  
Profitons-en pour paufiner le fichier de configuration de postfix **/etc/postfix/main.cf** :
  
```
smtpd_helo_required = yes
smtpd_helo_restrictions = permit_sasl_authenticated, reject_invalid_helo_hostname,
    reject_non_fqdn_helo_hostname

# Règles pour accepter ou refuser une connexion :
# - on attend une seconde (pour piéger les zombies) ;
# - on interdit la parallélisation là où il n'est pas sensé y en avoir.

smtpd_client_restrictions =
    permit_mynetworks, permit_sasl_authenticated,
    sleep 1, reject_unauth_pipelining

# Règles pour accepter ou refuser un message, dès lors qu'on connaît le nom
# de l'hôte de l'expéditeur (par sa commande HELO ou EHLO) :
# - on refuse les noms d'hôte invalides.
smtpd_helo_restrictions = reject_invalid_helo_hostname

# Règles pour accepter ou refuser un message, dès lors qu'on connaît l'adresse
# de l'expéditeur :
# - s'il vient d'un expéditeur inexistant de notre domaine, on le rejette ;
# - si le domaine de l'expéditeur n'a pas d'IP ou de MX, on le refuse ;
# - s'il vient d'un client sûr ou d'un client authentifié, on l'accepte ;
# - si l'adresse de l'expéditeur n'est pas sous forme canonique, on le refuse.
smtpd_sender_restrictions =
    reject_unlisted_sender, reject_unknown_sender_domain,
    permit_mynetworks, permit_sasl_authenticated,
    reject_non_fqdn_sender

# Règles pour accepter ou refuser un message, dès lors qu'on connaît le
# destinataire (par la commande RCPT TO) :
# - s'il est destiné à un expéditeur forgé chez nous, on le rejette ;
# - s'il est destiné à un domaine forgé, on le rejette ;
# - s'il vient d'un hôte sûr ou d'un client authentifié, on l'accepte ;
# - si l'adresse de destination n'est pas sous forme canonique, on le refuse ;
# - finalement, s'il n'est pas destiné à un domaine que l'on gère ou pour
#   lequel on relaie, on le refuse.
smtpd_recipient_restrictions =
    reject_unlisted_recipient, reject_unknown_recipient_domain,
    permit_mynetworks, permit_sasl_authenticated,
    reject_non_fqdn_recipient,
    reject_unauth_destination
```
  
Allez, on relance postfix pour qu&rsquo;il prenne en compte tout ça :
  
```
# /etc/init.d/postfix restart
```
