---
title: 'Ne pas laisser de trace dans l&rsquo;historique'
description: Effacer historique bash
author: JC
date: 2014-12-30T17:05:16+00:00
url: /ne-pas-laisser-de-trace-dans-lhistorique/
featured_image: /wp-content/uploads/2014/12/bash_history-604x270.gif
tasg:
  - Memos

---
Lorsque l&rsquo;on est plusieurs sur un système Linux, il peut arriver que l&rsquo;on n&rsquo;ait pas envie de laisser les commandes que l&rsquo;on a entrées dans le terminal.

Il est possible d&rsquo;éditer directement votre ~/.bash_history, ou effacer vos commandes à coups de history -d xxx, mais la méthode que je préfère est celle-ci:

```
HISTFILE=/dev/null
```

$HISTFILE est la variable d&rsquo;environnement contenant le fichier d&rsquo;historique. Vous pouvez la définir à n&rsquo;importe quel moment. Une fois définie à /dev/null, il vous suffit de vous déloguer pour que les commandes entrées lors de votre session soient perdues à jamais.
