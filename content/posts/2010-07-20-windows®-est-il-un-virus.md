---
title: Windows® est-il un virus ?
author: JC
date: 2010-07-20T20:37:54+00:00
url: /windows®-est-il-un-virus/
categories:
  - En vrac
tags:
  - Histoires

---
Analysons un peu le comportement des virus :

  * Ils se reproduisent rapidement &#8212; _OK, Windows®_ _le fait._
  * Les virus utilisent les ressources système pour ralentir l&rsquo;ordinateur &#8212; _OK__, Windows® le fait._
  * Les virus, de temps en temps, effacent votre disque dur &#8212; _OK__, Windows le fait aussi._
  * Habituellement les virus sont transportés avec des programmes utiles. _Sigh&#8230; Windows® le fait aussi._
  * Les virus font croire a l&rsquo;utilisateur que leur ordinateur est trop lent (cf point 2) et les utilisateurs changent de matériel. _Yup, encore Windows®._

On pourrait croire que Windows® est un virus mais il y a des différences fondamentales : les virus marchent sur la plupart des ordinateurs, le code de leur programme est rapide, compact et efficace et ils deviennent de plus en plus sophistiqués au fur et a mesure de leur évolution.

<span style="text-decoration: underline;"><em>Conclusion :</em></span>

**_Windows® n&rsquo;est donc pas un virus&#8230; ou plutôt si, mais même en tant que virus, il est mauvais.  
_** 

<p style="text-align: right;">
  <a href="http://membres.lycos.fr/homere/win/virus.html">source</a>
</p>