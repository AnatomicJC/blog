---
title: Emulation de jeux de bornes d'arcade
author: JC
date: 2010-07-21T13:18:37+00:00
url: /emulation-de-jeux-de-bornes-darcade/
tags:
  - geekeries

---
Mame est un émulateur permettant de jouer à de vieux jeux d&rsquo;arcades. J&rsquo;installe ici <strong>gmameui</strong> qui est un frontend graphique à <strong>sdlmame</strong> permettant de gérer ses roms, etc.
  
Installation :

```
# aptitude install gmameui
```
  
Il faut bien sûr posséder des roms pour jouer aux jeux…
  
Pour lancer une rom, il suffit de double-cliquer dessus.

## Configuration des touches

Lorsqu&rsquo;une rom est lancée, appuyer sur la touche <strong>tabulation</strong> pour pouvoir redéfinir les touches clavier (insert coin, gauche, droite, start, etc.).
