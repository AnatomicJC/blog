---
title: Installed packages from a specific repository
description: Get installed packages from a specific repository on Debian
author: JC
date: 2018-02-17T09:33:41+00:00
url: /installed-packages-from-a-specific-repository/
categories:
  - En vrac

---
Today a quick command line to know installed packages from a specific repository. If you want to know all installed packages on your machine from download.virtualbox.org:

``` bash
for package in $(dpkg -l | awk '{print $2}'); do apt-cache madison $package | grep download.virtualbox.org; done
```

Simple and easy ! Replace download.virtualbox.org with the repository you are searching for.
