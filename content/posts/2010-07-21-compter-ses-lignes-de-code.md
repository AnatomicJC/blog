---
title: Compter ses lignes de code
author: JC
date: 2010-07-21T13:04:14+00:00
url: /compter-ses-lignes-de-code/
categories:
  - En vrac

---
Un script permettant de compter le nombre total de lignes de code dans ses fichiers php : 

```
#!/bin/bash

liste=`find -name "*.php"`
var=0

for i in $liste; do
    ajout=`wc -l $i | awk '{print $1}'`
    var=`expr $var + $ajout`
done

echo $var
```

Voici la version en une ligne :

```
$ liste=`find -name "*.php"`;var=0; for i in $liste; do ajout=`wc -l $i | awk '{print $1}'`; var=`expr $var + $ajout` ; done; echo $var
```
