---
title: FFMPEG MP3 et image fixe
description: FFMPEG MP3 et image fixe
author: JC
date: 2011-06-19T19:44:29+00:00
url: /ffmpeg-mp3-et-image-fixe/
tags:
  - Memos

---
Yop,

Un rapide memo pour me souvenir de comment encoder un MP3 vers AVI.  
En fait, on place une image fixe.

```
ffmpeg -loop_input -f image2 -i image.png -r 1 -vcodec flv -i audio.mp3 -acodec copy -g 10 -qscale 2 -cmp 3 -subcmp 3 -mbd 2 -flags trell final.avi -shortest
```

Source: <http://forum.videohelp.com/threads/280695-FFMPEG-Loop-input-video>
