---
title: "Optimizing SQLite for servers"
author: JC
date: 2024-04-17T00:00:01+00:00
url: /optimizing-sqlite-for-servers
description: Optimizing SQLite for servers
cover:
  image: /images/databases/databases.jpg
tags:
  - servers
  - performance
  - sqlite

ShowToc: false
TocOpen: false
---

## TL;DR

```
PRAGMA journal_mode = WAL;
PRAGMA busy_timeout = 5000;
PRAGMA synchronous = NORMAL;
PRAGMA cache_size = 1000000000;
PRAGMA foreign_keys = true;
PRAGMA temp_store = memory;
```

Read the full article: [https://kerkour.com/sqlite-for-servers](https://kerkour.com/sqlite-for-servers)