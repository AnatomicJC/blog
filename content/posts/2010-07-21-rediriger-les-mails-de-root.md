---
title: Rediriger les mails de root
description: Rediriger les mails de root
author: JC
date: 2010-07-21T14:26:56+00:00
url: /rediriger-les-mails-de-root/
tags:
  - postfix

---
Sur un serveur, les mails sont souvent adressés à l&rsquo;utilisateur root, il est possible de les rediriger vers une adresse de son choix en faisant un alias.
  
Editer le fichier `/etc/aliases`
 
```
vim /etc/aliases
```
  
Ajouter l&rsquo;alias
  
```
root: mon_mail@mon_domaine.com
```
  
Recharger les alias
  
```
newaliases
```
  
Pour tester :
  
```
echo "hello world" | mail -s test root
```
