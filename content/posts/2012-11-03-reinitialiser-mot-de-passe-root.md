---
title: Réinitialiser mot de passe root
description: Réinitialiser mot de passe root
author: JC
date: 2012-11-03T10:43:09+00:00
url: /reinitialiser-mot-de-passe-root/
featured_image: /wp-content/uploads/2012/11/Root-Me-Please.jpg
tags:
  - Debian
  - Memos

---
Il peut arriver que l&rsquo;on se retrouve face à une machine dont on ne connait pas le root password (oubli ou tout simplement on n&rsquo;a jamais eu connaissance de ce mot de passe).

Voici comment le réinitialiser, à condition d&rsquo;avoir un accès physique à cette machine:

Au démarrage de la machine, lors de l&rsquo;affichage de Grub, appuyez sur la touche **e** pour éditer la ligne de boot.

![](/images/reinitialiser-mot-de-passe-root/Sélection_001.png "Menu de démarrage de Grub")

A la ligne « linux /boot/vmlinuz&#8230;. », ajoutez **init=/bin/sh** comme sur la capture ci-dessous:

![](/images/reinitialiser-mot-de-passe-root/Sélection_003.png "Edition de Grub")

Puis appuyez sur les touches **Ctrl et X** pour démarrer.

**init=/bin/sh **permet de vous donner directement un accès root à la machine, sans taper de mot de passe.

L&rsquo;inconvénient est que la partition est montée en lecture-seule, si vous voulez changer le mot de passe root à l&rsquo;aide de la commande **passwd**, vous aurez ce message d&rsquo;erreur:

![](/images/reinitialiser-mot-de-passe-root/Sélection_004.png)

Il faut pour celà monter la partition en lecture/écriture, ce qui ce fait facilement (vous êtes root !):

```
# mount -o remount rw /
```

Voilà, il ne vous reste plus qu&rsquo;à utiliser la commande **passwd** pour changer le mot de passe.

![](/images/reinitialiser-mot-de-passe-root/Sélection_005.png)
