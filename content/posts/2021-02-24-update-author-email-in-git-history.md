---
title: Update author and email in git history
description: Update author and email in git history
author: JC
date: 2021-02-24T14:41:02+00:00
url: /update-author-email-in-git-history/
tags:
  - git

---

Imagine you have already made some commits in a git repo and you want to update your mail address and/or your name ? In short, you want to re-write git history with good informations.

Here is a script who can do that. Put here in parent folder of git repo you want to edit:

```
#!/bin/sh

git filter-branch --env-filter '
OLD_EMAIL="incorrect-email@domain.tld"
CORRECT_NAME="Your Name"
CORRECT_EMAIL="good-email@domain.tld"
if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
  export GIT_COMMITTER_NAME="$CORRECT_NAME"
  export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
  export GIT_AUTHOR_NAME="$CORRECT_NAME"
  export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
```

This script will search for all commits done by `incorrect-email@domain.tld` and replace them by correct name and mail.

From the repo you want to edit, you can launch the script like this:

```
/bin/sh ../script.sh
```

You can now force push to re-write the history !

```
git push --force
```
