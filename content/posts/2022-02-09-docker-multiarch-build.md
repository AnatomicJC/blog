---
title: How to build a docker multiarch image with buildah
author: JC
date: 2022-02-09T10:00:00+00:00
url: /how-to-build-docker-multiarch-build/
description: build a docker multiarch image with buildah
tags:
  - docker
  - multiarch
  - buildah
  - Gitlab CI/CD

ShowToc: true
TocOpen: true
---

Let's say we want to build a custom multiarch image named **my-image**.

Very simple and minimalistic Dockerfile example:

```
FROM debian:latest
```

## With buildah

Login to docker hub or your private registry:

```
buildah login docker.io
```

Create a manifest, it is a kind of enveloppe who will contain your docker image with different architectures.

```
buildah manifest create my-manifest
```

Build images and store them in your manifest:

```
for PLATFORM in linux/386 linux/amd64 linux/arm64/v8 linux/arm/v5 linux/arm/v6 linux/arm/v7 linux/arm/v8 linux/s390x linux/ppc64le
do
  buildah bud --manifest my-manifest --platform ${PLATFORM}
done
```

> $PLATFORM variable use [$GOOS and $GOARCH variables, you can get a list by clicking here](https://go.dev/doc/install/source#environment)

If you inspect your manifest, you will see a json file with your images and their architectures:

```
buildah manifest inspect my-manifest
```

You can now push your manifest to docker hub and define a name and tag

* `--all`: Without this parameter, manifest will be pushed without the images
* `--format v2s2`: this format is mandatory to correctly display architectures in Docker Hub or Gitlab Registry. Default is oci.

```
buildah manifest push --all --format v2s2 my-manifest docker://docker.io/yourusername/yourimagename:yourtagname
```

###  Example image created with buildah:

[https://hub.docker.com/r/anatomicjc/busybox/tags](https://hub.docker.com/r/anatomicjc/busybox/tags)

![](/images/docker-multiarch/docker-multiarch.jpg)

## With Docker

Install some dependencies for Debian:

```
sudo apt install -y qemu qemu-user-static qemu-user binfmt-support
```

Login to docker hub or your private registry:

```
docker login docker.io
```

Refresh available archs:

```
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

Instanciate your buildx build:

```
docker buildx create --name multiarch --driver docker-container --use
```

Inspect your setup:

```
docker buildx inspect --bootstrap
```

Build your multiarch image:

```
docker buildx build -t yourusername/yourimagename:yourtagname --platform linux/s390x linux/ppc64le linux/arm64 linux/amd64 linux/arm/v7 linux/arm/v5 --push .
```

## Gitlab CI/CD

I created [this Gitlab repository](https://gitlab.com/AnatomicJC/docker-images) to automate multiarch docker images build. It is a kind of playground where I experiment stuffs.

This repository use [Gitlab CI/CD dynamic pipelines feature](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html#dynamic-child-pipelines). In practice, I just have to add a new Dockerfile and multiarch Gitlab pipeline is automagically generated.

Docker images are automatically created > tested > pushed to docker hub.

![](/images/docker-multiarch/gitlab-pipeline.jpg)
