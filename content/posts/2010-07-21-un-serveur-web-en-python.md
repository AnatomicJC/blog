---
title: Un serveur web en python
author: JC
date: 2010-07-21T08:42:37+00:00
url: /un-serveur-web-en-python/
tags:
  - python
  - Memos

---
_source : Linux Magazine_

On peut créer un petit serveur web en python. On utilise pour cela les modules **BaseHTTPServer** et **SimpleHTTPServer**.

```#!/usr/bin/env python

import os
from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler

os.chdir(os.path.expanduser("~/web"))

httpd = HTTPServer(('', 8080), SimpleHTTPRequestHandler)
httpd.serve_forever()
```

La classe HTTPServer est instanciée avec 2 paramètres :

  * le premier indique sous forme d&rsquo;un tuple sur quelle adresse et quel port le serveur doit écouter. Ici la chaîne est vide ce qui signifie que l&rsquo;on écoute sur toutes les adresses.
  * Le second est une classe qui sera instanciée avec 3 paramètres pour chaque requête et dont le rôle sera de taiter la requête en question.

Une fois le serveur web instancié, on lui demande répondre aux requêtes, c&rsquo;est le rôle de la dernière ligne.

La classe SimpleHTTPRequestHandler ne permet pas de choisir le répertoire qui sera servi, c&rsquo;est pourquoi on change de répertoire avant de servir les requêtes avec cette ligne :

```
os.chdir(os.path.expanduser("~/supersite"))
```

C&rsquo;est ici le répertoire **supersite** de votre répertoire personnel qui est utilisé.

Il faut rendre le script executable :

```
chmod +x serveurWeb.py
```

Puis le lancer :

```
./serveurWeb.py
```

Avec un navigateur, il suffit de se rendre à l&rsquo;adresse <a title="http://localhost:8080" rel="nofollow" href="http://localhost:8080/">http://localhost:8080</a> pour obtenir le contenu du répertoire **supersite** de votre répertoire personnel.

Par contre, ce n&rsquo;est pas le serveur du siècle, il n&rsquo;accepte qu&rsquo;une seule requête à la fois.
