---
title: La fibre à Maizières-Lès-Metz
description: La fibre à Maizières-Lès-Metz
author: JC
date: 2013-09-21T15:02:52+00:00
url: /la-fibre-a-maizieres-les-metz/
featured_image: /wp-content/uploads/2013/09/fibre-optique.jpg
categories:
  - En vrac
tags:
  - auto-hebergement
  - FTTH
  - wibox

---
Cet article vous dévoile mes premières émotions en tant que nouvel abonné éligible à la fibre à Maizières-Lès-Metz. Et un premier retour d&rsquo;expérience sur le FAI [Wibox][1].<!--more-->

Le serveur qui héberge ce site web est un PC portable qui se trouve planqué dans mon salon, derrière une vitrine. C&rsquo;est un Dell Inspiron 9300.

[<img loading="lazy" class="size-medium wp-image-759 aligncenter" alt="minou" src="https://jcvassort.open-web.fr/wp-content/uploads/2013/09/minou-168x300.jpg" width="168" height="300" srcset="https://jcvassort.open-web.fr/wp-content/uploads/2013/09/minou-168x300.jpg 168w, https://jcvassort.open-web.fr/wp-content/uploads/2013/09/minou.jpg 576w" sizes="(max-width: 168px) 100vw, 168px" />][2]

Je suis un adepte de l&rsquo;auto-hébergement, et tente de garder au maximum le contrôle de mes données sur le web. Les principaux services que j&rsquo;utilise sont:

  * hébergement de sites web
  * serveur de mails
  * serveur DNS
  * une instance d&rsquo;[etherpad][3]
  * vidéo-surveillance de la maison
  * Un bouncer IRC
  * etc.

Je ne vais pas tout lister&#8230;

# Free, parce que j&rsquo;ai tout compris

J&rsquo;ai toujours été chez Free car c&rsquo;était le FAI grand public des geeks. Il est possible de personnaliser son reverse DNS, d&rsquo;utiliser le service de téléphonie avec un client SIP&#8230; Mais surtout Free fourni une IP Fixe, ce qui est préférable pour héberger ses services chez soi. Mais il y a aussi le triple play, car j&rsquo;ai une femme et un enfant 🙂

# Dégradation des services

L&rsquo;auto-hébergement, c&rsquo;est cool, mais comme tout consommateur moyen, je vais sur YouTube mater des vidéos avec mon fils, et j&rsquo;ai besoin de faire mes updates d&rsquo;Android sur le Google Play Store. Pas de bol, [Free a déclaré la guerre à Google][4] et j&rsquo;ai observé depuis le début de l&rsquo;année une véritable dégradation du débit.

J&rsquo;allais prendre une offre ADSL chez OVH, il y a apparemment de bon retours d&rsquo;utilisateurs sur Twitter. C&rsquo;est neutre, pas de bridage comme chez Free, SFR ou Orange.  
L&rsquo;ADSL c&rsquo;est cool, mais le A de ADSL signifie Asymétrique et ça, ça pue car si on peut avoir de bons débit en download, l&rsquo;upload de 1 Mega est vraiment pourri.

# Puis arriva la Fibre !

C&rsquo;est [Resoptic][5] qui a déployé la fibre à Maizières et 4 opérateurs ont le droit de l&rsquo;exploiter.

Surprise ! que des inconnus pour moi&#8230; Pas de Orange, Free ou SFR&#8230;

J&rsquo;ai donc contacté ces 4 FAI afin de leur poser quelques questions, notamment sur l&rsquo;attribution ou pas d&rsquo;une IP fixe et les usages autorisés.

Sur les 4, un seul a pris la peine de me répondre, et cette opérateur, c&rsquo;est Wibox. Oui, une seule réponse sur les 4 🙁

En fait non, un second m&rsquo;a rappelé, mais au bout d&rsquo;un mois:

> FAI &#8211; Bravo monsieur, vous êtes éligible à la Fibre !
> 
> Moi &#8211; Mmmm&#8230; ça je le savais déjà&#8230; et la réponse à mes questions ? notamment sur l&rsquo;attribution ou pas d&rsquo;une IP Fixe ?
> 
> FAI &#8211; Bravo monsieur, vous êtes éligible à la Fibre !
> 
> Moi &#8211; Ce n&rsquo;est pas ce que je vous ai demandé, est-ce que vous pouvez répondre à mes questions ?
> 
> FAI &#8211; Non
> 
> Moi &#8211; au revoir

# Wibox, le FAI

J&rsquo;ai donc choisi Wibox, il y a maintenant 2 mois. Et je peux vous dire que je ne suis pas déçu. Bien au contraire.

Il n&rsquo;y a pas de bridage, et le réseau est neutre. Je peux regarder des vidéos Youtube en 1080p sans crainte. Je n&rsquo;ai plus peur d&rsquo;essayer des application sur le Google Play Store, je sais que j&rsquo;arriverai au bout du téléchargement sans réessayer 10 fois.

Côté auto-hébergement, rien à dire, ça fonctionne.

Quand j&rsquo;étais chez Free, mon serveur mail ne pouvait pas communiquer directement avec les serveurs de Hotmail ou Yahoo. Tout mail en provenance d&rsquo;une IP d&rsquo;un FAI grand public est refusé chez Yahoo. Chez Hotmail, les logs me disaient « Mail sent », mais en fait le destinataire ne recevait jamais mes mails. J&rsquo;étais donc obligé pour les adresse en @yahoo et @hotmail de passer par le smtp de Free. Saloperie de Yahoo/Hotmail !

Depuis que je suis chez Wibox, plus besoin, mes mails sont acceptés.

# Le débit !

Côté débit, j&rsquo;ai une véritable connexion 100M symétrique ! et ça c&rsquo;est un vrai plaisir !

Ci-dessous, je download un CD de Debian, puis je l&rsquo;upload vers un de mes serveurs OVH.

On peut observer que je dépasse les 10M/s autant en download qu&rsquo;en upload !

[<img loading="lazy" class="aligncenter size-medium wp-image-762" alt="Download / Upload" src="https://jcvassort.open-web.fr/wp-content/uploads/2013/09/Sélection_003-300x206.jpg" width="300" height="206" srcset="https://jcvassort.open-web.fr/wp-content/uploads/2013/09/Sélection_003-300x206.jpg 300w, https://jcvassort.open-web.fr/wp-content/uploads/2013/09/Sélection_003-435x300.jpg 435w, https://jcvassort.open-web.fr/wp-content/uploads/2013/09/Sélection_003.jpg 897w" sizes="(max-width: 300px) 100vw, 300px" />][6]

# Et l&rsquo;IP fixe ?

Wibox fournit des IP fixes uniquement pour les abonnement Pro. N&rsquo;étant pas dans mes moyens, j&rsquo;ai donc une IP dynamique.

Ce n&rsquo;est finalement pas si grave pour l&rsquo;hébergement de mes services car mon IP n&rsquo;a pas bougé depuis que mon abonnement est actif.

Et j&rsquo;ai trouvé également [un moyen de mettre à jour l&rsquo;IP de mon serveur @home sur mon serveur DNS sans avoir recours aux services de DynDNS ou NoIP.][7]

Toujours dans le but de dépendre le moins possible de services tiers, et d&rsquo;apprendre&#8230; car j&rsquo;aime ça !

# Wibox, un FAI accessible

Il y a quelques semaines, j&rsquo;ai reçu un courrier que tous les abonnés Wibox ont reçu. Un courrier type me remerciant tout d&rsquo;abord d&rsquo;avoir choisi Wibox, mais également pour me tenir au courant des derniers travaux réalisés sur l&rsquo;infrastructure. Ce courrier signé Thomas Gassilloud, président associé de Wibox était accompagné d&rsquo;un pseudo Twitter [@tgassilloud][8].

Je l&rsquo;ai twitté pour lui dire que Wibox je trouvais ça cool, et on a échangé quelques banalités. Je ne sais pas si vous avez déjà essayé de papoter avec [@xavier75][9]&#8230;

# D&rsquo;autres avis ?

Un « voisin », [@zast57][10], a également partagé un retour d&rsquo;expérience. Vous pouvez les consulter ici:

[Avis sur la fibre optique et le fournisseur d’accès Wibox][11]

[Avis et Tuto sur la Wibox TV Révélation][12]

 [1]: http://www.wibox.fr/
 [2]: https://jcvassort.open-web.fr/wp-content/uploads/2013/09/minou.jpg
 [3]: http://etherpad.org/
 [4]: http://www.20minutes.fr/high-tech/1074847-pourquoi-free-a-t-il-declare-guerre-a-google
 [5]: http://www.resoptic.fr/
 [6]: https://jcvassort.open-web.fr/wp-content/uploads/2013/09/Sélection_003.jpg
 [7]: http://tavie.onsenfout.com/2012/04/04/dns-dynamique-securise-avec-nsupdate-et-bind9/
 [8]: https://twitter.com/tgassilloud
 [9]: https://twitter.com/xavier75
 [10]: https://twitter.com/zast57
 [11]: http://paradoxetemporel.fr/4792-avis-sur-la-fibre-optique-et-le-fournisseur-dacces-wibox.html
 [12]: http://paradoxetemporel.fr/4844-avis-et-tuto-sur-la-wibox-tv-revelation.html
