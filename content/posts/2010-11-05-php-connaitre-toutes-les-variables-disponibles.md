---
title: 'PHP: connaître toutes les variables disponibles'
author: JC
date: 2010-11-05T14:28:49+00:00
url: /php-connaitre-toutes-les-variables-disponibles/
categories:
  - En vrac
tags:
  - PHP

---
En codant dans Drupal, j&rsquo;avais besoin de connaître toutes les variables disponibles de la page courante.

Méthode bourrin des familles mais qui fonctionne:

``` php
<?php var_dump($GLOBALS) ?>
```

Un peu de culture:

<a href=http://php.net/manual/fr/function.var-dump.php>var_dump()</a>  
<a href=http://php.net/manual/fr/reserved.variables.globals.php>$GLOBALS</a>
