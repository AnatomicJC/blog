---
title: Backup de jeux MegaCD
author: JC
date: 2010-08-16T17:27:10+00:00
url: /backup-de-jeux-megacd/
tags:
  - geekeries
  - python

---
Ci-dessous un petit script python trouvé sur [cette page][1] permettant de faire un backup d&rsquo;un jeu du [MegaCD de Sega][2].

Un CD est coupé en deux, il contient une partie donnée + une partie audio. Il faut donc ripper séparément les musiques et les datas. Certains jeux comme SonicCD ne peuvent fonctionner sans la  musique.

Pour y jouer sur PC, il suffit ensuite d&rsquo;utiliser un émulateur comme Gens.

```python
#!/usr/bin/python

import os
import commands
from sys import argv

# programs
SOURCE_DRIVE = "/dev/cdrom"
DD_COMMAND = "/bin/dd"
CDPARANOIA_COMMAND = "/usr/bin/cdparanoia --quiet"
LAME_COMMAND = "/usr/bin/lame --quiet --preset insane"
RM_COMMAND = "/bin/rm"

if (len(argv) &lt;2):
    print "USAGE: rip-sega-cd.py "
else:
    title = argv[1]
    print "ripping " + title

    # rip ISO-9660 filesystem in the first track
    data_rip_command = DD_COMMAND + " if=" + SOURCE_DRIVE + " of=\"" + title + ".iso\""

    print "ripping data track..."
    print data_rip_command

    commands.getstatusoutput(data_rip_command)

    print

    # rip CD audio tracks -> MP3 files until cdparanoia reports an error
    print "ripping audio tracks..."
    n = 2
    error = 0
    rm_command = RM_COMMAND + " cdda.wav"

    while (error == 0):
    # rip
    audio_rip_command = '%s %d' % (CDPARANOIA_COMMAND, n)

    print audio_rip_command
    commands.getstatusoutput(audio_rip_command)

    # encode
    audio_encode_command = '%s cdda.wav "%s %02d.mp3"' % (LAME_COMMAND, title, n)

    print audio_encode_command
    if (commands.getstatusoutput(audio_encode_command)[0]):
        error = 1

    # cleanup
    print rm_command

    commands.getstatusoutput(rm_command)
    n = n + 1

    print
```

 [1]: http://multimedia.cx/eggs/sega-cd-ripper/
 [2]: http://fr.wikipedia.org/wiki/Mega-CD
