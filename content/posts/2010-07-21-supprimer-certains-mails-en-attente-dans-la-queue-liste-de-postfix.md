---
title: Supprimer certains mails en attente dans la queue liste de Postfix
author: JC
date: 2010-07-21T14:29:44+00:00
url: /supprimer-certains-mails-en-attente-dans-la-queue-liste-de-postfix/
tags:
  - postfix

---
source : http://www.placeoweb.com/dotclear/index.php/2007/07/09/72-supprimer-certains-mails-en-attente-dans-la-queue-liste-de-postfix

Pour effacer un mail en attente de distribution sous Postfix, il faut utiliser :

```
# postsuper -d queue_id (delete)
```
  
Par exemple pour tous les effacer vous utiliserez :
  
```
# postsuper -d ALL
```
  
Mais il n&rsquo;y a pas d&rsquo;option pour supprimer plusieurs messages filtrés en batch.
  
Consultez a votre liste d&rsquo;attente avec :
  
```
# postqueue -p
```
  
qui vous listera quelque chose comme suit :
  
```
# 702472540B2     3744 Tue Jan  8 18:47:51  expediteur@du.domaine.com
```
  
D&rsquo;où le petit script suivant en shell : emptymailq.sh
  
```
#!/bin/sh
 
todel="du.domaine.com"
 
nbmsg=0
 
while test 1
do
 
liste=`postqueue -p | grep ^[0-9A-Z] | grep -v empty | grep $todel | cut -d \* -f1 | head -1`
 
if [ -n "$liste" ] ;
then
       echo Message a supprimer $liste
       listemsg=`echo $liste | cut -c- 11`
       postsuper -d $listemsg
else
       echo Nombre de message suprime $nbmsg
       exit 0
fi
let nbmsg="$nbmsg+1"
 
done
 
echo Nombre de message suprime $nbmsg
```
