---
title: 'Console de récupération XP > mot de passe refusé'
author: JC
date: 2010-09-12T10:28:52+00:00
url: /console-de-recuperation-xp-mot-de-passe-refuse/
tags:
  - Windows
  - Memos

---
Il peut arriver que vous vouliez accéder à la console de récupération de Windows XP. Seulement voilà, vous êtes sûr de votre mot de passe administrateur mais la console vous envoie bouler. Ça peut arriver.

La solution dans ce cas-là est de réinitialiser le mot de passe de l&rsquo;Administrateur:

Cliquer sur Démarrer > Executer et entrez la commande suivante:

```
control userpasswords2
```

Dans la fenêtre qui va s&rsquo;ouvrir, sélectionnez l&rsquo;administrateur et cliquez sur le bouton réinitialiser le mot de passe.
