---
title: "How to install packages on Red Hat"
author: JC
date: 2022-06-23T00:00:00+00:00
url: /how-to-install-packages-on-red-hat
description: How to install packages on Red Hat
cover:
  image: /images/redhat/redhat-logo.svg
tags:
  - redhat
  - memo
  - linux
  - setup

ShowToc: true
TocOpen: true
---

## The problem

Hey ! You have just installed a new Red Hat OS and you would like to start to play with but you are stuck with packages installation:

```shell
$ sudo dnf install nginx
Last metadata expiration check: 0:00:10 ago on Thu 23 Jun 2022 08:08:59 AM UTC.
No match for argument: nginx
Error: Unable to find a match: nginx
$ sudo dnf install mariadb
Last metadata expiration check: 0:00:26 ago on Thu 23 Jun 2022 08:08:59 AM UTC.
No match for argument: mariadb
Error: Unable to find a match: mariadb
```

> WTF ?? You cannot install anything ??

Red Hat is not like other Linux distros like Debian, Ubuntu, Rocky or Alma, you have to pay if you want to use it.

Each time you will use the `dnf` command, your OS will check if you have a valid subscription and allow you to install packages, or not :p

## Become a RedHat developer

If you plan to use your Red Hat for development (and only for development), you can join the Red Hat Developer program by creating a free account.

Go to [https://developers.redhat.com](https://developers.redhat.com), and at the very bottom, you will find a red button "Join Red Hat Developer":

![](/images/redhat/join-redhat-developer-program.jpg)

Click on it and create your account.

## Configuration

### Register

With your developer account, you can now register your OS (use your redhat login):

```shell
sudo subscription-manager register --username your-redhat-login
```

Will output: 

```shell
Registering to: subscription.rhsm.redhat.com:443/subscription
Password:
The system has been registered with ID: cb695c56-d85e-40cf-afa3-b6f15079fd09
The registered system name is: redhat9
```

### Refresh

Refresh the subscription manager:

```shell
sudo subscription-manager refresh
```

### Attach

And finally join with this command:

```shell
sudo subscription-manager attach --auto
```

Will output:

```shell
Installed Product Current Status:
Product Name: Red Hat Enterprise Linux for x86_64
Status:       Subscribed
```

## Enjoy !

You can now install packages on your RedHat:

```shell
sudo dnf install nginx
```

Will output:

```shell
Updating Subscription Management repositories.
Red Hat Enterprise Linux 9 for x86_64 - BaseOS (RPMs)   1.6 MB/s | 2.1 MB     00:01
Red Hat Enterprise Linux 9 for x86_64 - AppStream (RPMs 5.5 MB/s | 7.8 MB     00:01
Dependencies resolved.
========================================================================================
 Package             Arch    Version            Repository                         Size
========================================================================================
Installing:
 nginx               x86_64  1:1.20.1-10.el9    rhel-9-for-x86_64-appstream-rpms  607 k
Installing dependencies:
 nginx-filesystem    noarch  1:1.20.1-10.el9    rhel-9-for-x86_64-appstream-rpms   13 k
 redhat-logos-httpd  noarch  90.4-1.el9         rhel-9-for-x86_64-appstream-rpms   18 k

Transaction Summary
========================================================================================
Install  3 Packages

Total download size: 639 k
Installed size: 1.8 M
Is this ok [y/N]:  y
Downloading Packages:
(1/3): nginx-filesystem-1.20.1-10.el9.noarch.rpm         30 kB/s |  13 kB     00:00
(2/3): redhat-logos-httpd-90.4-1.el9.noarch.rpm          41 kB/s |  18 kB     00:00
(3/3): nginx-1.20.1-10.el9.x86_64.rpm                   1.0 MB/s | 607 kB     00:00
----------------------------------------------------------------------------------------
Total                                                   1.0 MB/s | 639 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                1/1
  Installing       : redhat-logos-httpd-90.4-1.el9.noarch                           1/3
  Running scriptlet: nginx-filesystem-1:1.20.1-10.el9.noarch                        2/3
  Installing       : nginx-filesystem-1:1.20.1-10.el9.noarch                        2/3
  Installing       : nginx-1:1.20.1-10.el9.x86_64                                   3/3
  Running scriptlet: nginx-1:1.20.1-10.el9.x86_64                                   3/3
  Verifying        : nginx-filesystem-1:1.20.1-10.el9.noarch                        1/3
  Verifying        : redhat-logos-httpd-90.4-1.el9.noarch                           2/3
  Verifying        : nginx-1:1.20.1-10.el9.x86_64                                   3/3
Installed products updated.

Installed:
  nginx-1:1.20.1-10.el9.x86_64              nginx-filesystem-1:1.20.1-10.el9.noarch
  redhat-logos-httpd-90.4-1.el9.noarch

Complete!
```

Please enjoy !
