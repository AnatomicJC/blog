---
title: "Git: track a remote branch"
author: JC
date: 2022-06-07T00:00:00+00:00
url: /git-track-remote-branch
description: git memo about how to track remote branch
tags:
  - git
  - memo

ShowToc: true
TocOpen: true
---

Here is the point:

* you have a git repository
* a random guy forked your repo and made a pull request
* You want to rework his contribution locally before merging it

On your local repo, add the random guy forked repository address and name it `random-guy`:

```
git remote add random-guy git@github.com:random-guy/your-project.git
```

You can see the remote repo you added with this command:

```
git remote show
```

Add the remote branch where the random guy did the pull request and name this branch `pr-rework` (for Pull Request rework). As this branch is on `random-guy` remote on `patch-1` branch, we will "track" `random-guy/patch-1` branch.

```
git checkout -b pr-rework --track random-guy/patch-1
```

You can now work on this branch, add new commits, and when you will merge it on your main branch, random guy pull request will be automatically closed.
