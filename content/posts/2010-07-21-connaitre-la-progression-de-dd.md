---
title: Connaître la progression de dd
description: Connaître la progression de dd
author: JC
date: 2010-07-21T14:41:40+00:00
url: /connaitre-la-progression-de-dd/
tags:
  - Memos
---
dd est pratique pour faire des dumps de disque mais on ne voit pas la progression en cours :
  
```
dd if=/dev/disqueABackuper of=imageDisque.img
```
  
Pour voir où en est la copie, il faut identifier le numéro de process, ouvrir une nouvelle console puis :
  
```
ps ax | grep dd
```
  
Une fois que l&rsquo;on a le numéro de processus de dd, faire dans cette console :
  
```
kill -USR1 "numeroDeProcess"
```
  
En dessous de la commande dd va apparaître la progression.
